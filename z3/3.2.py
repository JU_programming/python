#!usr/bin/env python3

if __name__ == '__main__':
    # funkcja sort() modyfikuje oryginalna L "in-place", a sama zwraca None
    # przez przypisanie L = L.sort() tracimy wiec posortowana liste
    L = [3, 5, 4]
    L = L.sort()
    print(L)

    # mamy za duzo wartosci w stosunku do zmiennych, dostaniemy blad ValueError
    # x, y = 1, 2, 3

    # nie mozna modyfikowac krotek w ten sposob, nie podlegaja one zmianom,
    # dostaniemy blad TypeError
    # X = 1, 2, 3
    # X[1] = 4

    # nie mozna modyfikowac listy poza jej zakresem, w tym przypadku zakres listy
    # miesci sie w przedziale 0-2, dostaniemy blad IndexError
    # X = [1, 2, 3]
    # X[3] = 4

    # string nie ma metody append, aby dodac dwa stringi uzywamy "+" (metoda tylko
    # dla kilku skladnikow) albo metody join(), w tym przypadku dostaniemy blad
    # AttributeError
    # X = "abc"
    # X.append("d")

    # dostaniemy tutaj blad TypeError, poniewaz funkcja pow wymaga dwoch argumentow:
    # jednego jako liczba, ktory podnosimy do jakiejs potegi, a drugi to potege
    # do ktorej podnosimy, range() jest traktowany jako jeden argument
    # L = list(map(pow, range(8)))
    #
    # przykladowe rozwiazanie, ktore zadziala
    L = list(map(pow, range(8), range(8)))
    print(L)
