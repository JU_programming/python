<h3>ZADANIE 7.1 (KLASA FRAC)</h3>

<p>W pliku <em>fracs.py</em> zdefiniować klasę Frac
wraz z potrzebnymi metodami.
Wykorzystać wyjątek <em>ValueError</em> do obsługi błędów w ułamkach.
Dodać możliwości dodawania|odejmowania|mnożenia|dzielenia|porównywania liczb (int, long)
do ułamków (działania lewostronne i prawostronne).
Rozważyć możliwość włączenia liczb float do działań na ułamkach.
Napisać kod testujący moduł <em>fracs</em>.

</p><hr><pre># Przykład zachowania klasy Fraction.
&lt;&lt;&lt; from fractions import Fraction
&lt;&lt;&lt; Fraction(3.14159)
Fraction(3537115888337719, 1125899906842624)
&lt;&lt;&lt; Fraction(3.14159).limit_denominator(1000) # default is 1000000
Fraction(355, 113)
&lt;&lt;&lt; Fraction(0.4)
Fraction(3602879701896397, 9007199254740992)
&lt;&lt;&lt; Fraction(0.4).limit_denominator(1000)
Fraction(2, 5)
</pre><hr><pre>class Frac:
    """Klasa reprezentująca ułamki."""

    def __init__(self, x=0, y=1):
        # Sprawdzamy, czy y=0.
        self.x = x
        self.y = y

    def __str__(self): pass         # zwraca "x/y" lub "x" dla y=1

    def __repr__(self): pass        # zwraca "Frac(x, y)"

    # Py2
    #def __cmp__(self, other): pass  # cmp(frac1, frac2)

    # Py2.7 i Py3
    def __eq__(self, other): pass

    def __ne__(self, other): pass

    def __lt__(self, other): pass

    def __le__(self, other): pass

    #def __gt__(self, other): pass

    #def __ge__(self, other): pass

    def __add__(self, other): pass  # frac1+frac2, frac+int

    __radd__ = __add__              # int+frac

    def __sub__(self, other): pass  # frac1-frac2, frac-int

    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        return Frac(self.y * other - self.x, self.y)

    def __mul__(self, other): pass  # frac1*frac2, frac*int

    __rmul__ = __mul__              # int*frac

    def __div__(self, other): pass  # frac1/frac2, frac/int, Py2

    def __rdiv__(self, other): pass # int/frac, Py2

    def __truediv__(self, other): pass  # frac1/frac2, frac/int, Py3

    def __rtruediv__(self, other): pass  # int/frac, Py3

    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self): pass         # -frac = (-1)*frac

    def __invert__(self): pass      # odwrotnosc: ~frac

    def __float__(self): pass       # float(frac)

    def __hash__(self):
        return hash(float(self))   # immutable fracs
        # w Pythonie set([2]) == set([2.0])
        # chcemy set([2]) == set([Frac(2)])

#Kod testujący moduł.

import unittest

class TestFrac(unittest.TestCase): pass
</pre><hr>

<h3>ZADANIE 7.3 (KLASA RECTANGLE)</h3>

<p>W pliku <em>rectangles.py</em> zdefiniować klasę Rectangle
wraz z potrzebnymi metodami.
Wykorzystać wyjątek <em>ValueError</em> do obsługi błędów.
Napisać kod testujący moduł <em>rectangles</em>.</p>

<hr><pre>
from points import Point

class Rectangle:
    """Klasa reprezentująca prostokąty na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
    # Chcemy, aby x1 &lt; x2, y1 &lt; y2.
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self): pass         # "[(x1, y1), (x2, y2)]"

    def __repr__(self): pass        # "Rectangle(x1, y1, x2, y2)"

    def __eq__(self, other): pass   # obsługa rect1 == rect2

    def __ne__(self, other):        # obsługa rect1 != rect2
        return not self == other

    def center(self): pass          # zwraca środek prostokąta

    def area(self): pass            # pole powierzchni

    def move(self, x, y): pass      # przesunięcie o (x, y)

    def intersection(self, other): pass # część wspólna prostokątów

    def cover(self, other): pass    # prostąkąt nakrywający oba

    def make4(self): pass           # zwraca krotkę czterech mniejszych
#A-------B   po podziale  A---+---B
#|       |                |   |   |
#|       |                +---+---+
#|       |                |   |   |
#D-------C                D---+---C

#Kod testujący moduł.

import unittest

class TestRectangle(unittest.TestCase): pass
</pre><hr>


<h3>ZADANIE 7.6 (ITERATORY)</h3>

<p>Stworzyć następujące iteratory nieskończone:
<br>(a) zwracający 0, 1, 0, 1, 0, 1, ...,
<br>(b) zwracający przypadkowo jedną wartość z ("N", "E", "S", "W")
[błądzenie przypadkowe na sieci kwadratowej 2D],
<br>(c) zwracający 0, 1, 2, 3, 4, 5, 6, 0, 1, 2, 3, 4, 5, 6, ...
[numery dni tygodnia].
</p>

