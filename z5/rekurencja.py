#!usr/bin/env python3

def check_input(n):
    if (not isinstance(n, int)) or n < 0:
        raise ValueError("Podaj liczbe naturalna")


def factorial(n):
    check_input(n)

    res = 1
    for i in range(2, n + 1):
        res *= i
    return res


def fibonacci(n):
    check_input(n)

    a, b = 0, 1
    for i in range(1, n + 1):
        a, b = b, a + b
    return a
