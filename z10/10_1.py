#!usr/bin/env python3

import tkinter as tk
from time import sleep
import random


def diceroll(label_name):
    for _ in range(50):
        num = random.randint(1, 6)
        label_name.config(text=num)
        label_name.update()
        sleep(0.02)


root = tk.Tk()
root.resizable(False, False)
root.title("App")
root.eval('tk::PlaceWindow . center')

dice = tk.PhotoImage(file="kostka.png")
image_label = tk.Label(root, image=dice)
image_label.image = dice

title_label = tk.Label(root, text="Roll 6-sided dice", font="Times 25")
label = tk.Label(root, text="Press any button", font="Times 25")
button1 = tk.Button(root, text="Roll the dice", command=lambda: diceroll(label), font="Times 15")
button2 = tk.Button(root, text="Quit", command=root.quit, font="Times 15")

root.rowconfigure(0, minsize=80)
root.rowconfigure(2, minsize=80)
root.columnconfigure(0, minsize=120)
root.columnconfigure(1, minsize=120)

title_label.grid(row=0, column=0, columnspan=2)
image_label.grid(row=1, column=0, columnspan=2)
label.grid(row=2, column=0, columnspan=2)
button1.grid(row=3, column=0, sticky="w")
button2.grid(row=3, column=1, sticky="e")

root.bind('<Return>', (lambda event: diceroll(label)))
root.bind('<Escape>', (lambda event: root.quit()))

root.mainloop()
