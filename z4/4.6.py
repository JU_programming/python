#!usr/bin/env python3

def sum_seq(sequence):
    res = 0
    for item in sequence:
        if isinstance(item, (list, tuple)):
            res += sum_seq(item)
        else:
            res += item
    return res


# alternatywnie
# def sum_seq2(seq):
#     if isinstance(seq, (list, tuple)):
#         return sum(map(sum_seq2(seq), seq))
#     else:
#         return seq


if __name__ == '__main__':
    seq = [[], [4], (1, 2), [3, 4], (5, 6, ([2, 3], 4, 1), 7)]
    expected_result = 42
    result = sum_seq(seq)
    assert result == expected_result
