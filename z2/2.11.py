#!usr/bin/env python3

if __name__ == '__main__':
    word = 'python'
    expected_result = 'p_y_t_h_o_n'
    # alternatywnie: print(*word, sep="_"), ale to tylko rzuca na ekran
    result = "_".join(word)
    assert result == expected_result