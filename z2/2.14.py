#!usr/bin/env python3

if __name__ == '__main__':
    line = 'Lorem ipsum dolor sit amet consectetur'

    expected_result = 'consectetur'
    expected_result2 = 11
    result = max(line.split(), key=len)
    result2 = len(result)
    assert result == expected_result
    assert result2 == expected_result2

    # alternatywnie:
    # gromadzenie wszystkich najdłuższych wyrazów w zbiorze:
    # set(word for word in line.split() if len(word) == max_len)
    # max_len, max_word = max((len(word), word) for word in line split())
