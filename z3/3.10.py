#!usr/bin/env python3

def roman2int(string):
    keys_ = ['I', 'IV', 'V', 'IX', 'X', 'XL', 'L', 'XC', 'C', 'CD', 'D', 'CM', 'M']
    values_ = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
    roman = dict(zip(keys_, values_))

    i = 0
    integer = 0
    while i < len(string):
        if i + 1 < len(string) and string[i:i+2] in roman:
            integer += roman[string[i:i+2]]
            i += 2
        else:
            try:
                integer += roman[string[i]]
            except KeyError:
                return f'To NIE jest liczba rzymska'
            else:
                i += 1

    return integer


if __name__ == '__main__':
    # rozne sposoby tworzenia slownika
    dict1 = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}

    dict2 = dict([('I', 1), ('V', 5), ('X', 10), ('L', 50), ('C', 100), ('D', 500),
                  ('M', 1000)])

    dict3 = dict(I=1, V=5, X=10, L=50, C=100, D=500, M=1000)

    keys = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
    values = [1, 5, 10, 50, 100, 500, 1000]

    dict4 = dict(zip(keys, values))

    dict5 = {}
    for (k, v) in zip(keys, values):
        dict5[k] = v

    D = [dict1, dict2, dict3, dict4, dict5]
    [print(f'{x}, typ: {type(x)}') for x in D]

    print(10*'=')
    r = 'V'
    print(f'{r}: {dict1.get(r)}')
    r = 'X'
    print(f'{r}: {dict1.get(r)}')
    print(10*'=')

    # ================================================================================

    S = ['III', 'CCLIV', 'MMMACCXXIV', 'MMMDCCXXIV']
    [print(f'{x}: {roman2int(x)}') for x in S]
