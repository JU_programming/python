#!usr/bin/env python3

"""
    Gra Snake.

    Zasady:
        - zdobadz jak najwieksza liczbe punktow w wyznaczonym czasie
        - zbieraj pozywne owoce (zielone kwadraty), aby zdobyc punkty
        - unikaj zatrutych owocow (czerwone kwadraty), poniewaz konczy to rozgrywke
        - uwazaj, aby waz sam sie nie zjadl, poniewaz konczy to rozgrywke
            (ruch wstecz rowniez zabroniony)

    Sterowanie:
        - strzalka w lewo: zmiana kierunku na lewo
        - strzalka w prawo: zmiana kierunku na prawo
        - strzalka w gore: zmiana kierunku na gore
        - strzalka w dol: zmiana kierunku na dol
        - q: wyjscie z gry
"""

import pygame
import random
import sys

# kolory
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (0, 255, 0)
blue = (50, 153, 213)

# rozdzielczosc gry
size = width, height = 600, 400

# waz
snake_block = 10  # tutaj mozna dostosowac rozmiar weza (pojedynczego segmentu)
snake_speed = 15  # tutaj mozna dostosowac predkosc weza
snake_length = 1
snake_list = []

# zegar
clock = pygame.time.Clock()
actual_time = 0
counter = -1
game_time = 420  # s, tutaj mozna dostosowac dlugosc rozgrywki (w sekundach)

# jedzenie
food_x = round(random.randrange(0, width - snake_block) / 10.0) * 10.0
food_y = round(random.randrange(0, height - snake_block) / 10.0) * 10.0
food_update_time = 5000  # 5s, tutaj mozna dostosowac maksymalna dlugosc zycia owocu
color_list = [green, red]  # czerwony kolor - zatruty owoc
actual_color = green

game_over = False
last_direction = -1
snake_x, snake_y = width / 2, height / 2  # koordynaty poczatkowe weza
x_change, y_change = 0, 0  # poczatkowa predkosc i kierunek poruszania sie weza

# gra
pygame.init()
screen = pygame.display.set_mode(size)
pygame.display.set_caption('Snake')

# czcionka
font = pygame.font.SysFont("comicsansms", 25)


def score(sc):
    value = font.render("Your Score: " + str(sc), True, yellow)
    screen.blit(value, [0, 0])


def remaining_time(c):
    remainingtime = game_time - (c // snake_speed)
    text = font.render("Time: {}".format(remainingtime), True, yellow)
    screen.blit(text, [width - 90, 0])
    return remainingtime


def snake(slist):
    for spiece in slist:
        pygame.draw.rect(screen, black, [spiece[0], spiece[1], snake_block, snake_block])


def message(msg, color):
    msg = font.render(msg, True, color)
    screen.blit(msg, [width / 3, height / 3])


def createfood():
    f_x = round(random.randrange(0, width - snake_block) / 10.0) * 10.0
    f_y = round(random.randrange(0, height - snake_block) / 10.0) * 10.0
    a_c = random.choices(color_list, weights=(80, 20))[0]
    return f_x, f_y, a_c


def closegame():
    pygame.quit()
    sys.exit(0)


while True:
    while game_over:
        screen.fill(blue)
        message("Game over! Press Q to Quit", red)
        score(snake_length - 1)
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_q):
                closegame()

    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_q):
            closegame()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                if last_direction == pygame.K_RIGHT:
                    game_over = True
                else:
                    x_change = -snake_block
                    y_change = 0
                    last_direction = pygame.K_LEFT
            elif event.key == pygame.K_RIGHT:
                if last_direction == pygame.K_LEFT:
                    game_over = True
                else:
                    x_change = snake_block
                    y_change = 0
                    last_direction = pygame.K_RIGHT
            elif event.key == pygame.K_UP:
                if last_direction == pygame.K_DOWN:
                    game_over = True
                else:
                    y_change = -snake_block
                    x_change = 0
                    last_direction = pygame.K_UP
            elif event.key == pygame.K_DOWN:
                if last_direction == pygame.K_UP:
                    game_over = True
                else:
                    y_change = snake_block
                    x_change = 0
                    last_direction = pygame.K_DOWN

    snake_x = (snake_x + x_change) % width
    snake_y = (snake_y + y_change) % height

    screen.fill(blue)
    pygame.draw.rect(screen, actual_color, [food_x, food_y, snake_block, snake_block])
    snake_head = [snake_x, snake_y]

    # dodajemy kolejna pozycje glowy weza, usuwamy nieaktualny ogon
    # (chyba, ze waz urosl, to zostawiamy ogon)
    snake_list.append(snake_head)
    if len(snake_list) > snake_length:
        del snake_list[0]

    for x in snake_list[:-1]:
        if x == snake_head:
            game_over = True

    snake(snake_list)
    score(snake_length - 1)

    counter += 1
    if remaining_time(counter) > 0:
        pygame.display.update()

        if snake_x == food_x and snake_y == food_y:
            if actual_color == red:
                game_over = True
            food_x, food_y, actual_color = createfood()
            actual_time = 0
            snake_length += 1

        actual_time += clock.get_time()
        if actual_time >= food_update_time:
            food_x, food_y, actual_color = createfood()
            actual_time = 0
    else:
        game_over = True

    clock.tick(snake_speed)
