<h3>ZADANIE 10.1</h3>

<p>Napisać program z GUI, który symuluje rzut kostką sześcienną.
Program powinien mieć przycisk uruchamiający rzut kostką
i etykietę wyświetlającą wynik (liczba od 1 do 6 lub odpowiedni obrazek).</p>
