<h3>ZADANIE 9.3 (SNAKE)</h3>

<p>Zaimplementować uproszczoną wersję gry Snake.
Wąż ma rozmiar jednego kwadratu, porusza się ruchem jednostajnym
w kierunku poziomym lub pionowym, zmiana kierunku (skręt) następuje
po naciśnięciu klawisza. Zabroniony jest ruch wstecz (koniec gry).
Zakładamy, że plansza ma periodyczne warunki brzegowe.

</p><p>Zadaniem gracza jest poruszanie wężem i zjadanie pożywnych owoców, 
które pojawiają się losowo na planszy i żyją określony czas.
W każdym momencie na planszy może być obecny najwyżej jeden owoc.
Gra kończy się po określonym czasie lub po niedozwolonym ruchu.
Wynik gry to stan licznika owoców.

</p><p>Można wprowadzić dwa rodzaje owoców, o różnych kolorach:
owoce pożywne i owoce zatrute. Po zjedzeniu zatrutego owocu można
karać gracza odjęciem punktów lub zakończyć grę.

</p><p>Można wprowadzić okresowe zwiększanie prędkości węża.

</p><p>Można wprowadzić wydłużanie węża po zjedzeniu pożywnego owocu,
a skracanie po zjedzeniu owocu zatrutego.

</p>
