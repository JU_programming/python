#!usr/bin/env python3

if __name__ == '__main__':
    line = 'Lorem ipsum dolor sit amet consectetur'

    expected_result = ['amet', 'consectetur', 'dolor', 'ipsum', 'Lorem', 'sit']
    expected_result2 = ['sit', 'amet', 'Lorem', 'ipsum', 'dolor', 'consectetur']

    result = sorted(line.split(), key=str.lower)
    assert result == expected_result

    result = sorted(line.split(), key=len)
    assert result == expected_result2

    # alternatywnie:
    # Py3.3+ key=str.casefold uwzglednia znaki narodowe
