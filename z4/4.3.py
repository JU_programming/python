#!usr/bin/env python3

def factorial(n):
    if (not isinstance(n, int)) or n < 0:
        raise ValueError("Podaj liczbe naturalna")

    res = 1
    for i in range(2, n + 1):
        res *= i
    return res


if __name__ == '__main__':
    expected_result = [1, 1, 120, 24]
    n_list = [0, 1, 5, 4]
    result = [factorial(n) for n in n_list]
    assert result == expected_result
    # print(factorial(-1))
