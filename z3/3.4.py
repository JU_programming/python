#!usr/bin/env python3

if __name__ == '__main__':
    while True:
        reply = input("Wpisz liczbe rzeczywista, aby poznac trzecia potege tej"
                      " liczby z dokladnoscia do 2 cyfr po przecinku. Napisz \"stop\", aby zakonczyc.\n")
        if reply == "stop":
            break
        try:
            number = float(reply)
        except ValueError:
            print("To nie jest liczba")
        else:
            print(f'({number})^3 = {round(number ** 3, 2)}')
