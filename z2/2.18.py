#!usr/bin/env python3

if __name__ == '__main__':
    number = 100060020506430503201092088703210
    expected_result = 14
    result = str(number).count('0')
    assert result == expected_result
