#!usr/bin/env python3

if __name__ == '__main__':
    # x = 2; y = 3;       # wiele deklaracji w jednej linii, niepotrzebny srednik na koncu
    # if (x > y):         # niepotrzebny nawias
    #     result = x;     # niepotrzebny srednik
    # else:
    #     result = y;     # niepotrzebny srednik

    # lepsze rozwiazanie
    x = 2
    y = 3
    if x > y:
        result = x
    else:
        result = y

    # albo
    x, y = 2, 3
    result = x if x > y else y

    # for i in "axby": if ord(i) < 100: print(i)
    #
    # nie mozna uzywac wielu dwukropkow w jednej linii, w tej sytuacji dostaniemy
    # blad SyntaxError, przy takich instrukcjach w jezyku python
    # wazne sa wciecia

    # rozwiazanie
    for i in "axby":
        if ord(i) < 100:
            print(i)

    print()

    # albo mozna uzyc wyrazenia trojargumentowego
    [print(i) for i in "axby" if ord(i) < 100]

    print(10*'=')

    # for i in "axby": print (ord(i) if ord(i) < 100 else i)
    #
    # tutaj nie bedzie bledu SyntaxError (tylko jeden dwukropek), jednak
    # miedzy print, a "(" nie powinien byc whitespace, a dodatkowo uzywajac
    # dwukropka powinnismy rozdzielac kod na osobne linie

    # rozwiazanie
    for i in "axby":
        print(ord(i) if ord(i) < 100 else i)

    print()

    # jezeli chcemy napisac wyrazenie w jednej linii, to z pomoca wyrazenia trojargumentowego
    [print(ord(i) if ord(i) < 100 else i) for i in "axby"]
