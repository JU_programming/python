#!usr/bin/env python3

if __name__ == '__main__':
    line = 'Lorem ipsum dolor sit amet consectetur'

    expected_result = 33
    result = sum(len(x) for x in line.split())
    assert result == expected_result

    # alternatywnie:
    # sum(map(len, line.split()))
    # char.isspace() - sprawdzenie, czy to jest spacja
    # sum(1 for char in line if not char.isspace())
