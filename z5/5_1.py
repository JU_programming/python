#!usr/bin/env python3

import rekurencja
import rekurencja as rek
from rekurencja import *
from rekurencja import factorial
from rekurencja import fibonacci as fib
from importlib import reload
from time import sleep

if __name__ == '__main__':
    print(rekurencja.factorial(6))
    print(rekurencja.fibonacci(5))

    print(rek.factorial(7))
    print(rek.fibonacci(8))

    print(factorial(2))
    print(fibonacci(3))

    print(fib(10))
    print(20*'=')

    # podczas trwania petli, gdy zmienimy funkcje fibonacci w rekurencja.py
    # i zastosujemy zmiany, od razu bedziemy to widziec w tym programie
    while True:
        reload(rekurencja)
        print(rekurencja.fibonacci(5))
        sleep(3)

