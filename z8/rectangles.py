#!usr/bin/env python3

from points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
        if x1 >= x2 or y1 >= y2:
            raise ValueError("The first point must be lower left point, the second upper right point")
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    @property
    def topleft(self):
        return Point(self.left, self.top)

    @property
    def bottomleft(self):
        return self.pt1

    @property
    def topright(self):
        return self.pt2

    @property
    def bottomright(self):
        return Point(self.right, self.bottom)
    
    @property
    def top(self):
        return self.topright.y

    @property
    def left(self):
        return self.bottomleft.x

    @property
    def bottom(self):
        return self.bottomleft.y

    @property
    def right(self):
        return self.topright.x

    @property
    def width(self):
        return self.right - self.left

    @property
    def height(self):
        return self.top - self.bottom
    
    @property
    def center(self):  # zwraca środek prostokąta
        return Point((self.left + self.right) / 2, (self.bottom + self.top) / 2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return f"[({self.left}, {self.bottom}), ({self.right}, {self.top})]"

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return f"Rectangle({self.left}, {self.bottom}, {self.right}, {self.top})"

    def __eq__(self, other):  # obsługa rect1 == rect2
        if not isinstance(other, Rectangle):
            return False

        return self.bottomleft == other.bottomleft and self.topright == other.topright

    def __ne__(self, other):  # obsługa rect1 != rect2
        return not self == other

    def area(self):  # pole powierzchni
        return self.width * self.height

    def move(self, x, y):  # przesunięcie o (x, y)
        self.pt1.x += x
        self.pt2.x += x
        self.pt1.y += y
        self.pt2.y += y

    def intersection(self, other):  # część wspólna prostokątów
        if self == other:
            return self
        elif self.area() > other.area():
            bigger_rect = self
            smaller_rect = other
        else:
            bigger_rect = other
            smaller_rect = self

        points = [smaller_rect.bottomleft, smaller_rect.topright, smaller_rect.topleft, smaller_rect.bottomright]

        for point in points:
            if (bigger_rect.left < point.x < bigger_rect.right) and (bigger_rect.bottom < point.y < bigger_rect.top):
                x1 = max(bigger_rect.left, smaller_rect.left)
                y1 = max(bigger_rect.bottom, smaller_rect.bottom)
                x2 = min(bigger_rect.right, smaller_rect.right)
                y2 = min(bigger_rect.top, smaller_rect.top)
                return Rectangle(x1, y1, x2, y2)

    def cover(self, other):  # prostokąt nakrywający oba
        x1 = min(self.left, other.left)
        y1 = min(self.bottom, other.bottom)
        x2 = max(self.right, other.right)
        y2 = max(self.top, other.top)
        return Rectangle(x1, y1, x2, y2)

    def make4(self):  # zwraca krotkę czterech mniejszych
        rec1 = Rectangle(self.left, self.bottom, self.center.x, self.center.y)
        rec2 = Rectangle(self.center.x, self.bottom, self.right, self.center.y)
        rec3 = Rectangle(self.left, self.center.y, self.center.x, self.top)
        rec4 = Rectangle(self.center.x, self.center.y, self.right, self.top)
        return rec1, rec2, rec3, rec4

    @classmethod
    def from_points(cls, points):
        pt1, pt2 = points
        if not (isinstance(pt1, Point) and isinstance(pt2, Point)):
            raise ValueError("The data must be points")
        return cls(pt1.x, pt1.y, pt2.x, pt2.y)
