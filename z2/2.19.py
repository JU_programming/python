#!usr/bin/env python3

if __name__ == '__main__':
    L = [75, 1, 154, 91, 0, 692, 7, 82, 44, 5, 528]
    expected_result = ['075', '001', '154', '091', '000', '692', '007', '082',
                       '044', '005', '528']
    result = [str(x).zfill(3) for x in L]
    assert result == expected_result

    # alternatywnie:
    # "".join("{0:03d}".format(x) for x in L)
    # "".join("{0:0>3}".format(x) for x in L)
    # "".join(f"{x:0>3}" for x in L)
