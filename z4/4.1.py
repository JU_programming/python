#!usr/bin/env python3

# wypisanie na terminal "qwerty", poniewaz X jest zmienna globalna.
# Gdy funkcja nie mogla znalezc zmiennej X lokalnie, szukala jej globalnie
# i potem wykorzystala zmienna globalna
# X = "qwerty"
#
# def func():
#     print(X)
#
# func()


# wypisanie na terminal "qwerty", poniewaz edycja byla na zmiennej lokalnej
# o tej samej nazwie. Funkcja znalazla zmienna lokalnie i nie szukala jej globalnie
# natomiast wypisanie odbylo sie juz poza funkcja, wiec wzieto zmienna globalna
# X = "qwerty"
#
# def func():
#     X = "abc"
#
# func()
# print(X)


# wypisanie na terminal "abc", poniewaz w funkcji zadeklarowano zmienna globalna,
# a potem ja zmieniono
# X = "qwerty"
#
# def func():
#     global X
#     X = "abc"
#
# func()
# print(X)
