# Implementacja grafu skierowanego/nieskierowanego wraz z przeszukiwaniem DFS i BFS


## Wprowadzenie

Celem powstania modułu ``graph.py`` było stworzenie implementacji grafów opartych na liście sąsiedztwa (lista list), których wierzchołkami są liczby typu int od `0` do `n-1`. 
W module ``graph.py`` znajdują się również zaimplementowane algorytmy przeszukiwania grafu wszerz (BFS) oraz przeszukiwania grafu w głąb (DFS).


### Podstawowe definicje

Grafem `G` nazywamy parę `(V,E)`, gdzie `V` to niepusty i skończony zbiór wierzchołków, natomiast `E` to skończony zbiór krawędzi, czyli par wierzchołków z `VxV`.

Wyróżnia się dwa główne typy grafów:

- Graf skierowany (ang. directed graph, digraph). Krawędź `(a,b)` ma kierunek od `a` do `b`, czyli jest to para uporządkowana.
Inne oznaczenia krawędzi to `a-b` lub `ab`.

- Graf nieskierowany (ang. undirected graph). Krawędź `(a,b)` jest równoważna krawędzi `(b,a)`, czyli pary wierzchołków nie są uporządkowane.
Można przyjąć, że w grafie nieskierowanym zawsze jednocześnie istnieją pary uporządkowane `(a,b)` i `(b,a)`.

Do reprezentacji grafu można wykorzystać różne struktury danych, takie jak macierz sąsiedztwa, lista sąsiedztwa, lista krawędzi
(w mojej implementacji wykorzystałem listę sąsiedztwa).

Lista sąsiedztwa (ang. adjacency-list) - każdemu wierzchołkowi `a` przyporządkowujemy listę wierzchołków, do których można dojść z wierzchołka `a`.
Reprezentacja ta jest wydajna dla rzadkich grafów z małą liczbą krawędzi. Wykorzystanie pamięci jest rzędu `O(V+E)`.


### Nazewnictwo

- Wierzchołek (vertex; vertices) lub węzeł (node).

- Krawędź (edge).

- Ścieżka (ang. path). Ścieżką długości `k` w grafie `G=(V,E)` nazywamy ciąg (zwykle różnych) wierzchołków `(v_0,v_1,...,v_k)` takich,
że krawędzie `(v_{i-1},v_i)` należą do `E` dla `i=1,2,...,k`.
Jest to ścieżka z wierzchołka `v_0` do wierzchołka `v_k`. Ścieżka zawiera wierzchołki i krawędzie.
Jeżeli istnieje ścieżka `P` z `a` do `b`, to mówimy, że wierzchołek `b` jest osiągalny z wierzchołka `a` po ścieżce `P`.

- Podgrafem (ang. subgraph) nazywamy taki podzbiór wierzchołków
i krawędzi danego grafu, który tworzy graf.

- Dwa grafy są izomorficzne (ang. isomorfic), jeżeli w wyniku
zamiany etykiet wierzchołków w jednym grafie otrzymamy układ
krawędzi identyczny z drugim.


### Grafy skierowane

- Pętla (ang. self-loop) jest to krawędź łącząca wierzchołek z samym sobą, czyli krawędź typu `(a,a)`.

- Graf prosty (ang. simple graph) nie ma pętli i krawędzi wielokrotnych.

- Krawędź `(a,b)` jest *wychodząca* z wierzchołka `a` i *wchodząca* do wierzchołka `b`.

- Stopień wejściowy wierzchołka (ang. indegree) jest liczbą krawędzi do niego wchodzących.

- Stopień wyjściowy wierzchołka (ang. outdegree) jest liczbą krawędzi wychodzących z niego.

- Stopniem wierzchołka w grafie skierowanym nazywamy liczbę będącą sumą jego stopni: wejściowego i wyjściowego.


### Grafy nieskierowane

- Stopień wierzchołka grafu (ang. degree or valency) jest to liczba krawędzi połączonych z tym wierzchołkiem.

- Dla danego grafu nieskierowanego możemy mówić o jego wersji skierowanej.
Jest to graf skierowany, gdzie każda krawędź nieskierowana `(a,b)==(b,a)` jest zastąpiona dwoma krawędziami skierowanymi `(a,b)` i `(b,a)`.
Zostało to wykorzystane w mojej implementacji grafu.


### Przykłady zastosowań grafów:

- Mapy — połączenia drogowe, kolejowe, lotnicze między miastami
- Dokumenty hipertekstowe
- Obwody elektryczne
- Planowanie — procesy produkcyjne, prace budowlane
- Transakcje — sieci rozmów telefonicznych, transakcje kupna-sprzedaży
- Sieci — sieci komputerowe, badanie odporności na uszkodzenia
- Struktura programu — struktura wywołań funkcji
- Relacje między ludźmi — zależności służbowe w firmie, znajomi na portalach społecznościowych
- Rekonstrukcja sekwencji DNA z fragmentów DNA 
- Segmentacja i analiza obrazów
- Generowanie i rozwiązywanie labiryntów
- Szeregowanie zadań wzajemnie zależnych


### Algorytm DFS

Funkcja ``dfs()`` realizuje przechodzenie w głąb przez graf (depth-first search, DFS) i ma strukturę rekurencyjną.
Zaczynamy od dowolnego węzła `start`, odwiedzamy go, a następnie rekurencyjnie odwiedzamy każdy nieodwiedzony węzeł dołączony do węzła `start`.

Jeśli graf jest spójny, wywołanie `dfs()` zdoła obejść wszystkie wierzchołki. Zbiór krawędzi, które przejdziemy metodą przechodzenia w głąb, tworzy drzewo rozpinające grafu.

Jeżeli graf jest drzewem, rekurencyjne przeszukiwanie grafu w głąb zaczynające się od korzenia jest równoważne przechodzeniu przez drzewo w porządku preorder.

Zamiast funkcji rekursywnej, można użyć while oraz stosu przechowującego wierzchołki.

Można również zaimplementować zmodyfikowaną wersję algorytmu DFS, który zdoła obejść wszystkie wierzchołki również dla grafu,
który **nie** jest spójny (patrz: [Algorytm DFS w rodziale: Implementacja](#dfs))

Złożoność czasowa DFS:
- każdy wierzchołek odwiedzany tylko raz
- w trakcie odwiedzania rozważane są wszystkie wychodzące krawędzie
- `O(n+m)` dla reprezentacji listami sąsiedztwa
- w grafach rzadkich `n ≈ m`, dla list sąsiedztwa wtedy `O(n)`


### Algorytm BFS

Funkcja `bfs()` realizuje przechodzenie wszerz (breadth-first search, BFS) i wykorzystuje kolejkę do przechowywania wierzchołków.
Algorytm przechodzi przez graf we wszystkich kierunkach. Przed przejściem z danego węzła do kolejnego odwiedza wszystkie połączone z nim węzły.

Złożoność czasowa BFS:
- każdy wierzchołek jeden raz przechodzi przez kolejkę
- podczas wyjmowania z kolejki rozważane są wszystkie wychodzące krawędzie
- taka sama złożoność jak dla DFS
- `O(n+m)` dla reprezentacji listowej, czyli `O(n)` gdy graf rzadki


## Interfejs

Interfejs modułu `graph.py` składa się z:

- klasy `Graph`

```python
class Graph:
    def __init__(self, isdirected: bool = True) -> None
        """Konstruktor klasy Graph"""

    def add_node(self, node: int) -> None
        """Wstawia wierzchołek do grafu"""

    def remove_node(self, node: int) -> None
        """Usuwa wierzchołek z grafu wraz z wszystkimi krawędziami z/do wierzchołka"""
    
    def add_edge(self, i: int, j: int) -> None
        """Dodaje krawędź pomiędzy wierzchołkami i oraz j (dla grafu nieskierowanego również w drugą stronę)"""

    def remove_edge(self, i: int, j: int) -> None
        """Usuwa krawędź pomiędzy wierzchołkami i oraz j (dla grafu nieskierowanego również w drugą stronę)"""

    def has_edge(self, i: int, j: int) -> bool
        """Sprawdza, czy krawędź pomiędzy wierzchołkami i oraz j istnieje"""

    def in_connections(self, node: int) -> set
        """Zwraca wszystkie wierzchołki, od których są połączenia do wierzchołka node"""

    def out_connections(self, node: int) -> set
        """Zwraca wszystkie wierzchołki, do których są połączenia z wierzchołka node"""

    def all_connections(self, node: int) -> set
        """Zwraca sumę zbiorów krawędzi wychodzących i wchodzących z/do node"""

    @property
    def nodes(self) -> list
        """Zwraca wszystkie wierzchołki w grafie"""

    @property
    def edges(self) -> list[tuple[int, int]]
        """Zwraca listę krawędzi (2-krotek) grafu"""

    def print_graph(self) -> None
        """Wypisuje postać grafu na ekranie"""

    @property
    def vertnumber(self) -> int
        """Zwraca liczbę wierzchołków w grafie"""

    def getgraph(self) -> dict[int, set]
        """Zwraca wierzchołki jako klucze wraz z wszystkimi wierzchołkami, do których wychodzą krawędzie jako zbiory"""

    @property
    def isdirected(self) -> bool
        """Sprawdza rodzaj grafu"""

    @classmethod
    def from_dict(cls, graph_dict: dict, isdirected: bool = True) -> Graph
        """Tworzy graf z podanego słownika"""

```

- algorytmu DFS

```python
def dfs(graph: Graph, startvert: int) -> list
    """Przeszukiwanie grafu w głąb, zwraca wierzchołki w kolejności przeglądania"""
    def dfs_visit(_g: {out_connections}, _v: int) -> None
        """Funkcja pomocnicza dla dfs, służy do przeglądania wierzchołków"""

```

- algorytmu BFS

```python
def bfs(graph: Graph, startvert: int) -> dict[int, int]
    """Przeszukiwanie grafu wszerz, zwraca słownik {węzeł: rodzic}"""
```


## Implementacja

Zostaną tutaj ujęte uwagi implementacyjne, które uznałem za najważniejsze.


### Ogólne uwagi

- Konstruktor klasy `Graph` przyjmuje tylko jeden argument ``isdirected``, który odpowiada za rodzaj grafu.
Domyślnie argument jest wartości ```True```, co oznacza, że graf jest skierowany. Aby stworzyć graf nieskierowany,``isdirected=False``

- Możemy tworzyć graf na dwa sposoby:
  - stworzyć graf przy pomocy konstruktora ``Graph()`` oraz ręcznie dodawać wierzchołki i krawędzie odpowiednio za pomocą
  ``add_node`` oraz ``add_edge``
  - stworzyć graf od razu z podanymi wierzchołkami oraz krawędziami za pomocą metody ``Graph.from_dict``, więcej patrz: [from_dict](#fromdict)

- Zamiast listy list, w implementacji użyto słownika, który jako klucze przyjmuje wartości typu ``int``,
natomiast jako wartości przyjmuje zbiory zawierające elementy typu ``int``.\
Nadal jest to lista sąsiedztwa, a zasada działania w obu przypadkach jest ta sama, jednak dzięki używaniu słownika oraz zbiorów, wykluczamy możliwość
powtarzających się elementów\
```graph = {0: set([1]), 1: set([2]), 2: set([0, 3]), 3: set([1])}```

- Pusty graf będzie reprezentowany przez pusty słownik.

- W podanej implementacji graf nieskierowany został potraktowany jak graf skierowany, gdzie dodawanie i usuwanie krawędzi
następuje *parami*, np. dla grafu nieskierowanego wywołanie ``graph.add_edge(0, 1)`` doda krawędzie `(0,1)` oraz `(1,0)`


### add_node

``add_node(node)`` dodaje ``{node: set()}`` do słownika grafu, tylko gdy wpis jeszcze nie istnieje.

```python
def add_node(self, node):
    """Wstawia wierzcholek do grafu

    :param node: wierzcholek do dodania
    :type node: int
    :return: None
    :rtype: None
    """
    if node not in self.__vec:
        self.__vec[node] = set()
```


### remove_node

``remove_node(node)`` najpierw szuka wszystkich krawędzi wchodzących do wierzchołka ``node``, by następnie je usunąć.
Po usunięciu krawędzi wchodzących usuwa ze słownika grafu odpowiedni wpis ``{node: {krawędzie_wychodzące}}``.

Wszystkie krawędzie wchodzące do wierzchołka ``node`` są zapisywane w zmiennej tymczasowej ``temp``,
aby uniknąć problemów związanych z dynamicznymi zmianami zbiorów w słowniku.

```python
def remove_node(self, node):
    """Usuwa wierzcholek z grafu wraz z wszystkimi krawedziami
    z/do wierzcholka

    :param node: wierzcholek do usuniecia
    :type node: int
    :return: None
    :rtype: None
    """
    if node in self.__vec:
        temp = self.in_connections(node).copy()
        [self.remove_edge(i, node) for i in temp]
        del self.__vec[node]
```


### add_edge

``add_edge(i, j)`` dodaje krawędź do słownika grafu, jeżeli wierzchołki ``i`` oraz ``j`` są wierzchołkami grafu
(inaczej rzucany jest wyjątek). Dla grafu skierowanego w słowniku dla klucza ``i``, do zbioru dodawany jest wierzchołek ``j``.
Dla grafu nieskierowanego dodatkowo dla klucza ``j``, do zbioru dodawany jest wierzchołek ``i`` (czyli dodawanie jest symetryczne).

```python
def add_edge(self, i, j):
    """Dodaje krawedz pomiedzy wierzcholkami i oraz j
    (dla grafu nieskierowanego rowniez w druga strone)

    :param i: wierzcholek poczatkowy krawedzi
    :type i: int
    :param j: wierzcholek koncowy krawedzi
    :type j: int
    :raises ValueError: Przynajmniej jeden z wierzcholkow nie nalezy do grafu
    :return: None
    :rtype: None
    """
    if i in self.__vec and j in self.__vec:
        self.__vec[i].add(j)
        if not self.__directed:
            self.__vec[j].add(i)
    else:
        raise ValueError("One of the vertices doesn't exist")
```


### remove_edge

``remove_edge(i, j)`` usuwa krawędź ze słownika grafu, jeżeli z wierzchołka ``i`` jest krawędź do wierzchołka ``j``
(w przypadku podania złych wierzchołków, patrz poniżej do **has_edge**). Dla grafu skierowanego w słowniku dla klucza ``i``, ze zbioru usuwany jest wierzchołek ``j``.
Dla grafu nieskierowanego dodatkowo dla klucza ``j``, ze zbioru usuwany jest wierzchołek ``i`` (czyli usuwanie jest symetryczne).

```python
def remove_edge(self, i, j):
    """Usuwa krawedz pomiedzy wierzcholkami i oraz j
    (dla grafu nieskierowanego rowniez w druga strone)

    :param i: wierzcholek poczatkowy krawedzi
    :type i: int
    :param j: wierzcholek koncowy krawedzi
    :type j: int
    :return: None
    :rtype: None
    """
    if self.has_edge(i, j):
        self.__vec[i].remove(j)
        if not self.__directed:
            self.__vec[j].remove(i)
```


### has_edge

``has_edge(i, j)`` sprawdza dwie rzeczy:
- czy wierzchołek ``i`` jest w grafie (czy jest poprawny)
- czy w zbiorze wierzchołka ``i`` jest wierzchołek ``j``.
Jeżeli wierzchołka ``j`` nie ma w zbiorze, oznacza to, że __albo__ krawędź ``(i, j)`` nie istnieje, __albo__ wierzchołek ``j`` nie istnieje (nie jest poprawny).

Dzięki temu można użyć tej funkcji do usunięcia krawędzi ``(i, j)``.

```python
def has_edge(self, i, j):
    """Sprawdza, czy krawedz pomiedzy wierzcholkami i oraz j istnieje

    :param i: wierzcholek poczatkowy krawedzi
    :type i: int
    :param j: wierzcholek koncowy krawedzi
    :type j: int
    :return: Informacje, czy dana krawedz istnieje
    :rtype: bool
    """
    return True if i in self.__vec and j in self.__vec[i] else False
```


### connections

Zebrane zostały tutaj 3 rodzaje połączeń:
- ``in_connections``
- ``out_connections``
- ``all_connections``


#### in_connections

``in_connections(node)`` sprawdza czy podany wierzchołek ``node`` istnieje w grafie (jeśli nie, rzucany jest wyjątek).
Następnie sprawdza, czy graf jest nieskierowany — dla grafów nieskierowanych krawędzie wchodzące do wierzchołka ``node``
są identyczne jak krawędzie z niego wychodzące. Można więc wtedy zwrócić krawędzie wychodzące.
Dla grafu skierowanego trzeba przeszukać wszystkie klucze i sprawdzić, czy w swoich zbiorach posiadają wierzchołek ``node``.
Jeśli jakiś klucz posiada, to taki klucz jest dodawany do zbioru wynikowego, który potem jest zwracany.

```python
def in_connections(self, node):
    """Zwraca wszystkie wierzcholki od ktorych sa polaczenia do wierzcholka node

    :param node: wierzcholek
    :type node: int
    :raises ValueError: Podano zly wierzcholek
    :return: Wszystkie wierzcholki, z ktorych krawedz prowadzi do node
    :rtype: set
    """
    if node in self.__vec:
        # Kiedy graf jest nieskierowany, krawedzie wychodzace i wchodzace do
        # danego wierzcholka sa takie same
        if not self.__directed:
            return self.out_connections(node)

        result = set()
        [result.add(i) for i in self.__vec if node in self.__vec[i]]
    else:
        raise ValueError("Vertex doesn't exist")

    return result
```


#### out_connections

``out_connections(node)`` zwraca wartość ze słownika grafu dla klucza ``node`` (czyli zwraca zbiór wierzchołków dla tego klucza).


#### all_connections

``all_connections(node)`` zwraca sumę zbiorów ``in_connections(node)`` oraz ``in_connections(node)``.


### properties

W klasie ``Graph`` dekorator ``@property`` posiadają:
- ``nodes`` — zwraca posortowaną listę kluczy, które są również wierzchołkami w grafie
- ``edges`` — zwraca listę krawędzi (krotek) postaci ``(klucz, wierzchołek_ze_zbioru[klucz])``,
np. dla grafu `{0: {1, 2}, 1: {2}, 2: set()}` zostanie zwrócona lista `[(0, 1), (0, 2), (1, 2)]`
- ``isdirected`` — zwraca wartość ``isdirected`` ustaloną podczas tworzenia grafu
- ``vertnumber`` — zwraca liczbę wierzchołków w grafie


### from_dict

``from_dict`` służy do szybszego tworzenia grafu, zamiast ręcznego dodawania wierzchołków i krawędzi
(oraz konieczności pilnowania przy dodawaniu krawędzi, czy wierzchołek początkowy i końcowy istnieją). ``from_dict`` przyjmuje:
- słownik z kluczami jako wierzchołkami oraz zbiorami wierzchołków końcowych dla każdego klucza: \
  `{klucz_1: {wierzchołek_końcowy_1, w_k_2, ..., w_k_n}, klucz_2: {wierzchołek_końcowy_1, w_k_2, ..., w_k_n}, ...}`
- parametr ``is_directed``, jak dla normalnego konstruktora (również domyślnie ustawiony na ```True```)

Dodatkowo ``from_dict`` wprowadza kolejne ułatwienia:
- nie ma różnicy, czy jako wartości w słowniku podamy **zbiory**, **listy** czy **pojedyncze liczby**, akceptowalne są różne formy:
  - `{0: 1, 1: set()}`
  - `{0: (1, 2), 1: {0, 2}, 2: set()}`
  - inne wariacje powyższych
- nie trzeba również definiować wszystkich wierzchołków jako kluczy, wystarczy określić tylko wierzchołki początkowe jako klucze
oraz wierzchołek końcowy w zbiorze wierzchołka początkowego, np. gdy podamy słownik ``graph_dict = {0: 1, 2: {4}}``, nasz graf będzie wyglądał następująco: \
``graph = {0: {1}, 1: set(), 2: {4}, 4: set()}``.\
W analogicznym przypadku tworzenia grafu przez konstruktor, przy dodawaniu krawędzi ``(0, 1)`` lub ``(2, 4)`` (__zakładając, że dodaliśmy tylko wierzchołki 0 oraz 2__),
rzucony zostanie błąd, że jeden z wierzchołków nie istnieje (przez co nie da się dodać krawędzi).

```python
def from_dict(cls, graph_dict, isdirected=True):
    """Tworzy graf z podanego slownika

    :param graph_dict: Slownik z kluczem jako wierzcholkiem oraz wartoscia
                jako zbiorem wierzcholkow, do ktorych jest krawedz z klucza
    :type graph_dict: dict
    :param isdirected: Okreslenie, czy graf jest skierowany
    :type isdirected: bool
    :raises TypeError: graph_dict nie jest slownikiem
    :return: nowy obiekt klasy Graph
    :rtype: Graph
    """
    if not isinstance(graph_dict, dict):
        raise TypeError("It is not a dictionary")

    g = cls(isdirected)
    for key in graph_dict.keys():
        g.add_node(key)
        # jesli to pojedyncza liczba, nie da sie przez nia iterowac,
        # wiec trzeba ja dodac od razu
        try:
            for value in graph_dict[key]:
                g.add_node(value)
                g.add_edge(key, value)
        except TypeError:
            g.add_node(graph_dict[key])
            g.add_edge(key, graph_dict[key])

    return g
```


### DFS

Algorytm DFS został opisany wcześniej (patrz: [Algorytm DFS](#algorytm-dfs)), tutaj skupię się na samej implementacji.

Możliwe są dwie implementacje DFS. Pierwsza z nich zakłada, że przechodzimy tylko po tych wierzchołkach,
do których istnieje ścieżka z wierzchołka startowego. Druga implementacja DFS zakłada, że przechodzimy po **wszystkich**
wierzchołkach grafu, zaczynając od wierzchołka startowego.

W tej implementacji użyto pierwszego sposobu.

```python
def dfs(graph, startvert):
    """Przeszukiwanie grafu w glab"""
    def dfs_visit(_g, _v):
        """Funkcja pomocnicza dla dfs, sluzy do przegladania wierzcholkow"""
        nonlocal visited_vert
        visited_vert.append(_v)
        for u in _g.out_connections(_v):
            if u not in visited_vert:
                dfs_visit(_g, u)

    if startvert not in graph.nodes:
        raise ValueError("Starting vertex doesn't exist")

    visited_vert = []
    dfs_visit(graph, startvert)

    return visited_vert
```

W drugim sposobie, przed zwróceniem listy z odwiedzonymi wierzchołkami,
jest również pętla po tych wierzchołkach w grafie, do których nie ma ścieżki z wierzchołka startowego.


W pseudokodzie ta pętla wygląda tak:
```python
for nieodwiedzony_wierzcholek in graph:
    dfs(nieodwiedzony_wierzcholek)
```

Różnicę zobaczymy na prostym przykładzie:

Załóżmy, że mamy prosty graf `G=(V,E)` z wierzchołkami `V=(0, 1, 2, 3)` oraz krawędziami `E=[(0, 1), (1, 3)]`.
- Dla pierwszego sposobu (który jest zaimplementowany w `graph.py`), funkcja `dfs(0)` zwróci listę `[0, 1, 3]`
- Dla drugiego sposobu funkcja `dfs(0)` zwróci listę `[0, 1, 3, 2]`


### BFS

Algorytm BFS został opisany wcześniej (patrz: [Algorytm BFS](#algorytm-bfs)), tutaj skupię się na samej implementacji.

W przeciwieństwie do ``dfs()``, ``bfs()`` zwraca słownik, gdzie kluczem jest wierzchołek, natomiast wartością jest jego rodzic.
Do `bfs()` użyto ``deque`` (double-ended queue) z modułu ``collections`` — do kolejki wierzchołki dodawane są na koniec,
a z kolejki wyciągany jest wierzchołek *najstarszy* (jest to kolejka typu FIFO — First In, First Out).

```python
def bfs(graph, startvert):
    """Przeszukiwanie grafu wszerz

    :param graph: graf, ktory chcemy przejrzec
    :type graph: Graph
    :param startvert: wierzcholek poczatkowy
    :type startvert: int
    :raises ValueError: podany wierzcholek nie nalezy do grafu
    :return: Slownik z wierzcholkami jako kluczami i rodzicami jako wartosciami
    :rtype: dict
    """
    if startvert not in graph.nodes:
        raise ValueError("Starting vertex doesn't exist")

    parent = {startvert: None}
    q = collections.deque()
    q.append(startvert)

    while q:
        vert = q.popleft()
        for node in graph.out_connections(vert):
            if node not in parent:  # wyszukiwanie O(1)
                parent[node] = vert
                q.append(node)

    return parent
```


## Podsumowanie

Podczas pisania kodu zwracano uwagę na to, by używać programowania obiektowego (OOP) oraz pisać zgodnie z PEP8 i PEP257.
Prawdopodobnie niektóre rzeczy można było zrobić prościej i wydajniej, ale wydaje się, że kod działa poprawnie.

Podczas testowania modułu wszystkie wykryte błędy zostały poprawione.
Nie wykluczam jednak tego, że jakiś błąd mógł mi umknąć (choć nie powinien). Proszę o zgłaszanie wykrytych błędów, aby móc je jak najszybciej poprawić.

Oprócz modułu ``graph.py``, w folderze znajdują się ``test_graph.py`` z testami, których między innymi używałem do testowania modułu
oraz ``example.py`` z przykładowym użyciem modułu.


## Źródła
- dr hab. Andrzej Kapanowski, materiały z wykładów z przedmiotu: Język Python.\
Materiały są dostępne na stronie: https://ufkapano.github.io/algorytmy/index.html
- dr hab. Anna Paszyńska, materiały z wykładów z przedmiotu: Algorytmy i Struktury Danych.
