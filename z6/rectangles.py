#!usr/bin/env python3

from points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return f"[({self.pt1.x}, {self.pt1.y}), ({self.pt2.x}, {self.pt2.y})]"

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return f"Rectangle({self.pt1.x}, {self.pt1.y}, {self.pt2.x}, {self.pt2.y})"

    def __eq__(self, other):  # obsługa rect1 == rect2
        if not isinstance(other, Rectangle):
            return False

        return self.pt1 == other.pt1 and self.pt2 == other.pt2

    def __ne__(self, other):        # obsługa rect1 != rect2
        return not self == other

    def center(self):  # zwraca środek prostokąta
        return Point((self.pt1.x + self.pt2.x) / 2, (self.pt1.y + self.pt2.y) / 2)

    def area(self):  # pole powierzchni
        return (self.pt2.x - self.pt1.x) * (self.pt2.y - self.pt1.y)

    def move(self, x, y):  # przesunięcie o (x, y)
        self.pt1.x += x
        self.pt2.x += x
        self.pt1.y += y
        self.pt2.y += y

# Kod testujący moduł.

import unittest


class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.r1 = Rectangle(0, 0, 3, 4)
        self.r2 = Rectangle(0, 0, 3, 4)
        self.r3 = Rectangle(0, 0, 5, 5)
        self.r4 = Rectangle(1, 2, 3, 4)

    def test_str(self):
        self.assertEqual(str(self.r1), "[(0, 0), (3, 4)]")
        self.assertEqual(str(self.r3), "[(0, 0), (5, 5)]")
        self.assertEqual(str(self.r4), "[(1, 2), (3, 4)]")

    def test_repr(self):
        self.assertEqual(repr(self.r1), "Rectangle(0, 0, 3, 4)")
        self.assertEqual(repr(self.r3), "Rectangle(0, 0, 5, 5)")
        self.assertEqual(repr(self.r4), "Rectangle(1, 2, 3, 4)")

    def test_eq(self):
        self.assertEqual(self.r1, self.r1)
        self.assertEqual(self.r1, self.r2)
        self.assertNotEqual(self.r1, self.r3)
        self.assertNotEqual(self.r3, self.r4)

    def test_center(self):
        self.assertEqual(self.r1.center(), Point(1.5, 2))
        self.assertEqual(self.r3.center(), Point(2.5, 2.5))
        self.assertEqual(self.r4.center(), Point(2, 3))
        self.assertNotEqual(self.r4.center(), Point(1, 2))

    def test_area(self):
        self.assertEqual(self.r1.area(), 12)
        self.assertNotEqual(self.r2.area(), 10)
        self.assertEqual(self.r3.area(), 25)
        self.assertEqual(self.r4.area(), 4)

    def test_move(self):
        self.r1.move(2, 3)
        self.r1.move(2, 3)
        self.r3.move(2, 3)
        self.r4.move(2, 3)
        self.assertNotEqual(self.r1, Rectangle(2, 3, 5, 7))
        self.assertEqual(self.r1, Rectangle(4, 6, 7, 10))
        self.assertEqual(self.r3, Rectangle(2, 3, 7, 8))
        self.assertEqual(self.r4, Rectangle(3, 5, 5, 7))


if __name__ == '__main__':
    unittest.main()
