#!usr/bin/env python3

def flatten(sequence):
    res = []
    for item in sequence:
        if isinstance(item, (list, tuple)):
            res.extend(flatten(item))
        else:
            res.append(item)
    return res


# szybsza wersja, jedna lista globalna
# def flatten2(seq, total=None):
#     if total is None:
#         total = []
#     for item in seq:
#         if isinstance(item, (list, tuple)):
#             flatten2(item, total)
#         else:
#             total.append(item)
#     return total

# jawne wykorzystanie stosu
# def flatten3(seq):
#     result = []
#     stack = []
#     for item in seq:
#         stack.append(item)
#     while len(stack) > 0:
#         item = stack.pop()
#         if isinstance(item, (list, tuple)):
#             for item2 in item:
#                 stack.append(item2)
#         else:
#             result.append(item)
#     result.reverse()
#     return result


if __name__ == '__main__':
    seq = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]
    expected_result = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    result = flatten(seq)
    assert result == expected_result

    seq2 = [[], [4], (1, 2), [3, 4], (5, 6, ([2, 3], 4, 1), 7)]
    expected_result2 = [4, 1, 2, 3, 4, 5, 6, 2, 3, 4, 1, 7]
    result2 = flatten(seq2)
    assert result2 == expected_result2
