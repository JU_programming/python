#!usr/bin/env python3

def odwracanie(L, left, right):
    if not (isinstance(left, int) and isinstance(right, int)):
        raise ValueError("Podaj liczbe naturalna")

    if -1 < left < right < len(L):
        while left < right:
            tmp = L[left]
            L[left] = L[right]
            L[right] = tmp
            # dziala tez L[left], L[right] = L[right], L[left]
            left += 1
            right -= 1


def odwracanie_rekurencja(L, left, right):
    if not (isinstance(left, int) and isinstance(right, int)):
        raise ValueError("Podaj liczbe naturalna")

    if -1 < left < right < len(L):
        tmp = L[left]
        L[left] = L[right]
        L[right] = tmp
        # dziala tez L[left], L[right] = L[right], L[left]
        odwracanie_rekurencja(L, left + 1, right - 1)

# gdyby nie polecenie, zamiast wlasnej funkcji mozna uzyc reverse
# L[left:right+1] = reverse(L[left:right+1])


if __name__ == '__main__':
    List = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
    expected_result = [0, 2, 4, 7, 5, 3, 1, 8, 6, 9]
    odwracanie(List, 3, 8)
    assert List == expected_result
    # odwracanie(List, 'a', 8)

    List2 = [0, 2, 4, 7, 5, 3, 1, 8, 6, 9]
    expected_result2 = [0, 3, 5, 7, 4, 2, 1, 8, 6, 9]
    odwracanie_rekurencja(List2, 1, 5)
    assert List2 == expected_result2
