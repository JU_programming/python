<h3>ZADANIE 2.10</h3>

<p>Mamy dany napis wielowierszowy <em>line</em>.
Podać sposób obliczenia liczby wyrazów w napisie.
Przez wyraz rozumiemy ciąg "czarnych" znaków, oddzielony
od innych wyrazów białymi znakami (spacja, tabulacja, newline).

<h3>ZADANIE 2.11</h3>

<p>Podać sposób wyświetlania napisu <em>word</em> tak,
aby jego znaki były rozdzielone znakiem podkreślenia.

<h3>ZADANIE 2.12</h3>

<p>Zbudować napis stworzony z pierwszych znaków wyrazów z wiersza <em>line</em>.
Zbudować napis stworzony z ostatnich znaków wyrazów z wiersza <em>line</em>.

<h3>ZADANIE 2.13</h3>

<p>Znaleźć łączną długość wyrazów w napisie <em>line</em>.

<h3>ZADANIE 2.14</h3>

<p>Znaleźć: (a) najdłuższy wyraz, (b) długość najdłuższego
wyrazu w napisie <em>line</em>.

<h3>ZADANIE 2.15</h3>

<p>Na liście L znajdują się liczby całkowite dodatnie.
Stworzyć napis będący ciągiem cyfr kolejnych liczb z listy L.

<h3>ZADANIE 2.16</h3>

<p>W tekście znajdującym się w zmiennej <em>line</em>
zamienić ciąg znaków "GvR" na "Guido van Rossum".

<h3>ZADANIE 2.17</h3>

<p>Posortować wyrazy z napisu <em>line</em> raz alfabetycznie,
a raz pod względem długości.

<h3>ZADANIE 2.18</h3>

<p>Znaleźć liczbę cyfr zero w dużej liczbie całkowitej.

<h3>ZADANIE 2.19</h3>

<p>Na liście L mamy liczby jedno-, dwu- i trzycyfrowe dodatnie. Chcemy zbudować
 napis z trzycyfrowych bloków, gdzie liczby jedno- i dwucyfrowe będą miały blok
dopełniony zerami, np.  007, 024. 

