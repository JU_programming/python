#!usr/bin/env python3
import math
import unittest

####################################################################
# Gdyby te funkcje były w klasie, wtedy mozna byloby pominac       #
# skracanie ulamkow wejsciowych przy kazdej funkcji                #
# (sprawdzanie odbywaloby sie w konstruktorze)                     #
# tutaj uzytkownik moze wpisac takze nieskrocony lub bledny ulamek #
####################################################################


def check_and_normalize(*fracs):
    for frac in fracs:
        if len(frac) != 2:
            raise ValueError("Fractions must have argument with 2 params")
        elif frac[1] == 0:
            raise ValueError("Second param cannot be 0")

        if frac[1] < 0:
            frac[0], frac[1] = -frac[0], -frac[1]
        nwd = math.gcd(frac[0], frac[1])
        # //, poniewaz / zwraca float
        frac[0], frac[1] = frac[0] // nwd, frac[1] // nwd


def add_frac(frac1, frac2):
    check_and_normalize(frac1, frac2)
    res = [frac1[0] * frac2[1] + frac2[0] * frac1[1], frac1[1] * frac2[1]]
    check_and_normalize(res)
    return res  # frac1 + frac2


def sub_frac(frac1, frac2):
    check_and_normalize(frac1, frac2)
    res = [frac1[0] * frac2[1] - frac2[0] * frac1[1], frac1[1] * frac2[1]]
    check_and_normalize(res)
    return res  # frac1 - frac2


def mul_frac(frac1, frac2):
    check_and_normalize(frac1, frac2)
    res = [frac1[0] * frac2[0], frac1[1] * frac2[1]]
    check_and_normalize(res)
    return res  # frac1 * frac2


def div_frac(frac1, frac2):
    check_and_normalize(frac1, frac2)
    if is_zero(frac2):
        raise ZeroDivisionError("Division by 0")
    res = [frac1[0] * frac2[1], frac1[1] * frac2[0]]
    check_and_normalize(res)
    return res  # frac1 / frac2


def is_positive(frac):
    check_and_normalize(frac)
    if frac[0] > 0:
        return True
    else:
        return False  # bool, czy dodatni


def is_zero(frac):
    check_and_normalize(frac)
    return frac[0] == 0  # bool, typu [0, x]


def cmp_frac(frac1, frac2):
    check_and_normalize(frac1, frac2)
    f1 = frac1[0] * frac2[1]
    f2 = frac2[0] * frac1[1]
    if f1 > f2:
        return 1
    elif f1 < f2:
        return -1
    else:
        return 0  # -1 | 0 | +1

    # alternatywnie
    # frac = sub_frac(frac1, frac2)
    # if is_zero(frac):
    #     return 0
    # elif is_positive(frac):
    #     return 1
    # else:
    #     return -1


def frac2float(frac):
    check_and_normalize(frac)
    return frac[0] / frac[1]  # konwersja do float


# f1 = [-1, 2]      # -1/2
# f2 = [1, -2]      # -1/2 (niejednoznaczność)
# f3 = [0, 1]       # zero
# f4 = [0, 2]       # zero (niejednoznaczność)
# f5 = [3, 1]       # 3
# f6 = [6, 2]       # 3 (niejednoznaczność)
#
# zastosowane rozwiazanie: uzyskanie jednoznacznosci przez funkcje check_and_normalize


class TestFractions(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]
        self.frac1 = [8, 12]
        check_and_normalize(self.frac1)
        self.fractions = ([2, 4], [-2, 6], [1, -5], [-5, -10], [0, 2])
        [check_and_normalize(f) for f in self.fractions]

    def test_second_param(self):
        self.assertRaises(ValueError, check_and_normalize, [5, 0])
        self.assertEqual(self.frac1, [2, 3])

    def test_wrong_frac_length(self):
        self.assertRaises(ValueError, check_and_normalize, [1, 3, 5])
        self.assertRaises(ValueError, check_and_normalize, [5])

    def test_check_and_normalize(self):
        self.assertEqual(self.fractions, ([1, 2], [-1, 3], [-1, 5], [1, 2], [0, 1]))

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])
        self.assertEqual(add_frac([2, 4], [9, 15]), [11, 10])
        self.assertEqual(add_frac(self.frac1, self.zero), self.frac1)

    def test_sub_frac(self):
        self.assertEqual(sub_frac([2, 3], [1, 3]), [1, 3])
        self.assertEqual(sub_frac([11, 10], [1, 2]), [3, 5])
        self.assertEqual(sub_frac([-1, 2], [11, 10]), [-8, 5])
        self.assertEqual(sub_frac(self.zero, self.frac1), [-2, 3])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([2, 3], [3, 5]), [2, 5])
        self.assertEqual(mul_frac([1, 2], [3, 5]), [3, 10])
        self.assertEqual(mul_frac([100, 2], [3, -50]), [-3, 1])
        self.assertEqual(mul_frac(self.zero, self.frac1), self.zero)

    def test_div_frac(self):
        self.assertEqual(div_frac([1, 2], [1, 3]), [3, 2])
        self.assertEqual(div_frac([1, 2], [3, 5]), [5, 6])
        self.assertRaises(ZeroDivisionError, div_frac, [4, 9], self.zero)

    def test_is_positive(self):
        self.assertTrue(is_positive([1, 2]))
        self.assertFalse(is_positive([-3, 12]))
        self.assertFalse(is_positive([2, -5]))

    def test_is_zero(self):
        self.assertTrue(is_zero(self.zero))
        self.assertFalse(is_zero([3, 5]))
        self.assertTrue(is_zero([0, 34]))

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([1, 2], [1, 3]), 1)
        self.assertEqual(cmp_frac([5, 20], [1, 4]), 0)
        self.assertEqual(cmp_frac([3, 5], [3, 4]), -1)

    def test_frac2float(self):
        self.assertAlmostEqual(frac2float([2, 7]), 2 / 7)
        self.assertAlmostEqual(frac2float([1, 5]), 0.2)
        self.assertAlmostEqual(frac2float([1000, 3000]), 1 / 3)
        self.assertEqual(frac2float([4, 2]), 2.0)

    def tearDown(self):
        del self.zero
        del self.frac1
        del self.fractions


if __name__ == '__main__':
    unittest.main()
