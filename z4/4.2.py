#!usr/bin/env python3

def make_ruler(n):
    if n > 9999:
        raise ValueError("Miarka przy podanej dlugosci stanie sie nieczytelna."
                         " Wybierz mniejsza liczbe")
    else:
        ruler = ["....".join("|" * (n + 1)), "\n0"]
        ruler.extend(str(i + 1).rjust(5) for i in range(n))
        ruler.append("\n")
        return "".join(ruler)


def make_grid(rows, cols):
    if rows * cols > 500:
        raise ValueError("W trosce o terminal, ograniczono powierzchnie"
                         " prostokata do 500 jednostek")
    else:
        pattern1 = "---".join(['+'] * (cols + 1)) + '\n'
        pattern2 = "   ".join(['|'] * (cols + 1)) + '\n'
        rectangle = pattern2.join([pattern1] * (rows + 1))
        return rectangle


if __name__ == '__main__':
    print(make_ruler(9))
    print(make_ruler(15))
    # print(make_ruler(10000))
    print(f"{80 * '='}\n")
    print(make_grid(3, 8))
    print(make_grid(4, 2))
    # print(make_grid(8, 100))
