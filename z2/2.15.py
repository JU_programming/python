#!usr/bin/env python3

if __name__ == '__main__':
    L = [1, 8, 7, 10, 99, 2, 3, 2, 15, 5]
    expected_result = '1223578101599'
    L.sort()
    result = "".join(str(x) for x in L)
    assert result == expected_result
