#!usr/bin/env python3

if __name__ == '__main__':
    S1 = set("mrowisko")
    S2 = set("ognisko")
    result1 = S1 & S2
    expected_result1 = {'o', 'i', 's', 'k'}
    assert result1 == expected_result1
    result2 = S1 | S2
    expected_result2 = {'m', 'r', 'o', 'w', 'i', 's', 'k', 'g', 'n'}
    assert result2 == expected_result2

    # jezeli obiekty nie sa hashowalne, trzeba iterowac
    # list1 = "mrowisko"
    # list2 = "ognisko"
    # wspolne = []
    # for item in list1:
    #     if item in list2 and item not in wspolne:
    #         wspolne.append(item)
    # wszystkie = []
    # for item in list1 + list2:
    #     if item not in wszystkie:
    #         wszystkie.append(item)
    #
    # print(f"wspolne: {wspolne}\nwszystkie: {wszystkie}")
