#!usr/bin/env python3

def fibonacci(n):
    if (not isinstance(n, int)) or n < 0:
        raise ValueError("Podaj liczbe naturalna")

    a, b = 0, 1
    for i in range(1, n + 1):
        a, b = b, a + b
    return a


if __name__ == '__main__':
    expected_result = [0, 1, 1, 3, 2, 21, 144, 55]
    n_list = [0, 1, 2, 4, 3, 8, 12, 10]
    result = [fibonacci(n) for n in n_list]
    assert result == expected_result
    # print(fibonacci(-1))
