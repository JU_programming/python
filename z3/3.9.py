#!usr/bin/env python3

if __name__ == '__main__':
    seq = [[], [3], (5, 4), [1, 9], (5, 6, 7), (2, 10, 8, 3), [1, 1, 2, 9, 0, 5]]
    expected_result = [0, 3, 9, 10, 18, 23, 18]
    result = [sum(x) for x in seq]
    assert result == expected_result

    # alternatywnie
    # result = list(map(sum, seq))
