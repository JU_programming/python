#!usr/bin/env python3
from graph import Graph, dfs, bfs

if __name__ == '__main__':
    graph1 = Graph(isdirected=False)

    graph1.add_node(0)
    graph1.add_node(1)
    graph1.add_node(3)
    graph1.add_node(5)
    graph1.add_edge(0, 1)
    graph1.add_edge(1, 3)
    graph1.add_edge(5, 3)

    print(f"Is there an edge (1, 3)?: {graph1.has_edge(1, 3)}")
    print(f"Is graph1 directed?: {graph1.isdirected}")
    print(f"graph1 nodes: {graph1.nodes}")
    print(f"graph1 edges: {graph1.edges}")
    print("graph1:")
    print(graph1.getgraph())
    graph1.print_graph()
    print(f"dfs(graph1, 0): {dfs(graph1, 0)}")

    print(80 * "=")

    graph_dict = {0: 1, 1: {2}, 5: (1, 3, 3)}
    graph2 = Graph.from_dict(graph_dict)  # isdirected = True
    print("graph2:")
    graph2.print_graph()

    print("Removing node 1 from graph 2 ...")
    graph2.remove_node(1)
    graph2.print_graph()
    print("Restoring node 1 with edges...")
    graph2.add_node(1)
    graph2.add_edge(0, 1)
    graph2.add_edge(1, 2)
    graph2.add_edge(5, 1)

    print(f"graph2 out connections from node 1: {graph2.out_connections(1)}")
    print(f"graph2 in connections to node 1: {graph2.in_connections(1)}")
    print(f"graph2 all connections with node 1: {graph2.all_connections(1)}")
    print(f"dfs(graph2, 0): {dfs(graph2, 0)}")
    print(f"bfs(graph2, 0): {bfs(graph2, 0)}")
