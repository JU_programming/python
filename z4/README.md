</p><h3>ZADANIE 4.1</h3>

<p>Jaki będzie wynik poniższego kodu i dlaczego?

</p><hr><pre>X = "qwerty"

def func():
    print(X)

func()
</pre><hr><pre>X = "qwerty"

def func():
    X = "abc"

func()
print(X)
</pre><hr><pre>X = "qwerty"

def func():
    global X
    X = "abc"

func()
print(X)
</pre><hr>

<h3>ZADANIE 4.2</h3>

<p>Rozwiązania zadań 3.5 i 3.6 z poprzedniego zestawu
zapisać w postaci funkcji, które zwracają pełny string
przez <em>return</em>.
Funkcje nie powinny pytać użytkownika o dane, tylko korzystać z argumentów.

</p><hr><pre>def make_ruler(n): pass

def make_grid(rows, cols): pass
</pre><hr>

<h3>ZADANIE 4.3</h3>

<p>Napisać iteracyjną wersję funkcji <em>factorial(n)</em> obliczającej silnię.

<h3>ZADANIE 4.4</h3>

<p>Napisać iteracyjną wersję funkcji <em>fibonacci(n)</em>
obliczającej n-ty wyraz ciągu Fibonacciego.

<h3>ZADANIE 4.5</h3>

<p>Napisać funkcję <em>odwracanie(L, left, right)</em> 
odwracającą kolejność elementów na liście od numeru left do right włącznie. 
Lista jest modyfikowana w miejscu (in place).
Rozważyć wersję iteracyjną i rekurencyjną.

<h3>ZADANIE 4.6</h3>

<p>Napisać funkcję  <em>sum_seq(sequence)</em>
obliczającą sumę liczb zawartych w sekwencji, 
która może zawierać zagnieżdżone podsekwencje.

<h3>ZADANIE 4.7</h3>

<p>Mamy daną sekwencję, w której niektóre z elementów mogą
okazać się podsekwencjami, a takie zagnieżdżenia mogą się
nakładać do nieograniczonej głębokości.
Napisać funkcję <em>flatten(sequence)</em>, która zwróci
spłaszczoną listę wszystkich elementów sekwencji.

</p><pre>seq = [1,(2,3),[],[4,(5,6,7)],8,[9]]
print(flatten(seq))   # [1,2,3,4,5,6,7,8,9]
</pre>

