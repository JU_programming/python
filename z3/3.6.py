#!usr/bin/env python3

if __name__ == '__main__':
    row, col = 3, 8

    if row * col > 500:
        print("W trosce o terminal, ograniczono powierzchnie prostokata")
    else:
        pattern = ["---".join(['+'] * (col + 1)), "   ".join(['|'] * (col + 1))]

        # print((row * "{0}\n{1}\n" + "{0}").format(pattern[0], pattern[1]))
        rectangle = row * (pattern[0] + '\n' + pattern[1] + '\n') + pattern[0]
        print(rectangle)

        # alternatywna wersja
        # row1 = "---".join("+" * (col + 1)) + "\n"
        # row2 = "   ".join("|" * (col + 1)) + "\n"
        # print(row2.join([row1] * (row + 1)))
