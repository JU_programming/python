#!usr/bin/env python3

import copy
from rectangles import Rectangle
from points import Point

import pytest


# Kod testujący moduł.
class TestRectangles:
    @pytest.fixture(scope="class")
    def r1(self):
        return Rectangle(0, 0, 3, 4)

    @pytest.fixture(scope="class")
    def r2(self):
        return Rectangle(0, 0, 3, 4)

    @pytest.fixture(scope="class")
    def r3(self):
        return Rectangle(0, 0, 5, 5)

    @pytest.fixture(scope="class")
    def r4(self):
        return Rectangle(1, 2, 3, 4)

    def test_params(self):
        pytest.raises(ValueError, Rectangle, 1, 2, 2, 2)
        pytest.raises(ValueError, Rectangle, 1, 3, 2, 2)
        pytest.raises(ValueError, Rectangle, 1, 2, 0, 2)
        pytest.raises(ValueError, Rectangle, 1, 2, 1, 3)
        pytest.raises(ValueError, Rectangle, 1, 2, 1, 2)
        pytest.raises(ValueError, Rectangle.from_points, (Point(1, 2), (5, 6)))

    def test_str(self, r1, r3, r4):
        assert str(r1) == "[(0, 0), (3, 4)]"
        assert str(r3) == "[(0, 0), (5, 5)]"
        assert str(r4) == "[(1, 2), (3, 4)]"

    def test_repr(self, r1, r3, r4):
        assert repr(r1) == "Rectangle(0, 0, 3, 4)"
        assert repr(r3) == "Rectangle(0, 0, 5, 5)"
        assert repr(r4) == "Rectangle(1, 2, 3, 4)"

    def test_eq(self, r1, r2, r3, r4):
        assert r1 == r1
        assert r1 == r2
        assert r1 != r3
        assert r3 != r4

    def test_properties(self, r1, r2, r3, r4):
        assert r1.center == Point(1.5, 2)
        assert r2.topleft == Point(0, 4)
        assert r3.bottomleft == Point(0, 0)
        assert r4.topright == Point(3, 4)
        assert r1.bottomright == Point(3, 0)
        assert r2.top == 4
        assert r3.left == 0
        assert r4.bottom == 2
        assert r1.right == 3
        assert r2.width == 3
        assert r3.height == 5

    def test_area(self, r1, r2, r3, r4):
        assert r1.area() == 12
        assert r2.area() != 10
        assert r3.area() == 25
        assert r4.area() == 4

    def test_move(self, r1, r3, r4):
        _r1, _r3, _r4 = copy.deepcopy(r1), copy.deepcopy(r3), copy.deepcopy(r4)
        _r1.move(2, 3)
        _r1.move(2, 3)
        _r3.move(2, 3)
        _r4.move(2, 3)
        assert _r1 != Rectangle(2, 3, 5, 7)
        assert _r1 == Rectangle(4, 6, 7, 10)
        assert _r3 == Rectangle(2, 3, 7, 8)
        assert _r4 == Rectangle(3, 5, 5, 7)

    def test_intersect(self, r1, r2, r3, r4):
        assert r1.intersection(r2) == r1
        assert r1.intersection(r3) == r1
        assert r3.intersection(r4) == r4
        assert Rectangle(0, 0, 1, 1).intersection(Rectangle(1, 1, 3, 3)) is None
        assert r4.intersection(Rectangle(8, 9, 15, 16)) is None
        assert r1.intersection(Rectangle(1, 2, 5, 6)) == r4
        assert Rectangle(1, 2, 5, 6).intersection(r1) == r4

    def test_cover(self, r1, r2, r3, r4):
        assert r1.cover(r2) == r1
        assert r1.cover(r3) == r3
        assert r3.cover(r1) == r3
        assert Rectangle(0, 0, 1, 1).cover(Rectangle(2, 2, 3, 3)) == Rectangle(0, 0, 3, 3)
        assert Rectangle(-2, 0, 1, 2).cover(Rectangle(1, 0, 3, 2)) == Rectangle(-2, 0, 3, 2)

    def test_make4(self, r1, r4):
        assert r1.make4() == (Rectangle(0, 0, 1.5, 2), Rectangle(1.5, 0, 3, 2),
                              Rectangle(0, 2, 1.5, 4), Rectangle(1.5, 2, 3, 4))
        assert r4.make4() == (Rectangle(1, 2, 2, 3), Rectangle(2, 2, 3, 3),
                              Rectangle(1, 3, 2, 4), Rectangle(2, 3, 3, 4))

    def test_from_points(self):
        p1 = Point(5, 10)
        p2 = Point(6, 11)
        p3 = Point(-5, -4)
        p4 = Point(0, -2)
        assert Rectangle.from_points((p1, p2)) == Rectangle(5, 10, 6, 11)
        assert Rectangle.from_points((p3, p4)) == Rectangle(-5, -4, 0, -2)
        assert Rectangle.from_points((p3, p2)) == Rectangle(-5, -4, 6, 11)


if __name__ == '__main__':
    pytest.main()
