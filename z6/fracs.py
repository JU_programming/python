#!usr/bin/env python3

import math


class Frac:
    """Klasa reprezentująca ułamek."""

    def __init__(self, x=0, y=1):
        if y == 0:
            raise ValueError("Second param cannot be 0")

        self.x = x
        self.y = y
        self.normalize()

    def __str__(self):
        if self.y == 1:
            return f"{self.x}"
        return f"{self.x}/{self.y}"  # zwraca "x/y" lub "x" dla y=1

    def __repr__(self):
        return f"Frac({self.x}, {self.y})"  # zwraca "Frac(x, y)"

    def __eq__(self, other):
        if isinstance(other, Frac):
            return self.x == other.x and self.y == other.y
        return float(self) == other  # Py2.7 i Py3, ==

    def __ne__(self, other):
        return not self == other  # !=

    def __lt__(self, other):
        if isinstance(other, Frac):
            return (self - other).x < 0
        return float(self) < other  # <

    def __le__(self, other):
        return self < other or self == other  # <=

    # nadmiarowe
    def __gt__(self, other):
        return not self <= other  # >

    # nadmiarowe
    def __ge__(self, other):
        return not self < other  # >=

    def __add__(self, other):
        return Frac(self.x * other.y + other.x * self.y, self.y * other.y)  # frac1 + frac2

    def __sub__(self, other):
        return Frac(self.x * other.y - other.x * self.y, self.y * other.y)  # frac1 - frac2

    def __mul__(self, other):
        return Frac(self.x * other.x, self.y * other.y)  # frac1 * frac2

    def __div__(self, other):
        if other.is_zero():
            raise ZeroDivisionError("Division by 0")
        return Frac(self.x * other.y, self.y * other.x)  # frac1 / frac2, Py2

    __truediv__ = __div__  # frac1 / frac2, Py3

    def __floordiv__(self, other):
        return Frac((self.x * other.y) // (self.y * other.x))  # frac1 // frac2

    def __mod__(self, other):
        return Frac((self.x * other.y) % (other.x * self.y), self.y * other.y)  # frac1 % frac2

    # operatory jednoargumentowe
    def __pos__(self):
        return self  # +frac = (+1)*frac

    def __neg__(self):
        return Frac(-self.x, self.y)  # -frac = (-1)*frac

    def __invert__(self):  # odwrotnosc: ~frac
        return Frac(self.y, self.x)

    def __float__(self):
        return self.x / self.y  # float(frac)

    def __hash__(self):
        return hash(float(self))  # immutable fracs

    def normalize(self):
        if self.y < 0:
            self.x = -self.x
            self.y = -self.y

        nwd = math.gcd(self.x, self.y)
        self.x //= nwd
        self.y //= nwd

    def is_positive(self):
        return True if self.x > 0 else False  # bool, czy dodatni

    def is_zero(self):
        return self.x == 0  # bool


# Kod testujący moduł.
import unittest


class TestFrac(unittest.TestCase):

    def setUp(self):
        self.zero = Frac(0, 1)
        self.frac1 = Frac(8, 12)
        self.fractions = (Frac(2, 4), Frac(-2, 6), Frac(4, 2), Frac(1, -5),
                          Frac(-5, -10), Frac(0, 2))

    def test_second_param(self):
        self.assertRaises(ValueError, Frac, 5, 0)
        self.assertEqual(self.frac1, Frac(2, 3))
        self.assertNotEqual(Frac(5, -7).y, -7)

    def test_frac_length(self):
        self.assertRaises(TypeError, Frac, 1, 3, 5)
        self.assertEqual(Frac(), self.zero)
        self.assertEqual(Frac(5), Frac(5, 1))

    def test_str(self):
        self.assertEqual([str(x) for x in self.fractions],
                         ["1/2", "-1/3", "2", "-1/5", "1/2", "0"])

    def test_repr(self):
        self.assertEqual([repr(x) for x in self.fractions],
                         ["Frac(1, 2)", "Frac(-1, 3)", "Frac(2, 1)", "Frac(-1, 5)",
                          "Frac(1, 2)", "Frac(0, 1)"])

    def test_eq(self):
        self.assertTrue(set([2]) == set([Frac(2)]))
        self.assertTrue(Frac(2, 3) == self.frac1)
        self.assertFalse(Frac(3, 4) == Frac(3, 5))
        self.assertFalse(Frac(2, 3) == 1.5)

    def test_ne(self):
        self.assertFalse(set([2]) != set([Frac(2)]))
        self.assertFalse(Frac(2, 3) != self.frac1)
        self.assertTrue(Frac(3, 4) != Frac(3, 5))
        self.assertTrue(Frac(2, 3) != 1.5)

    def test_lt(self):
        self.assertFalse(self.frac1 < Frac(3, 8))
        self.assertFalse(self.frac1 < Frac(2, 3))
        self.assertTrue(self.frac1 < Frac(1, 1))
        self.assertTrue(Frac(2, 3) < 1.5)
        self.assertFalse(Frac(3, 2) < 1.5)
        self.assertFalse(Frac(5, 2) < 1.5)

    def test_le(self):
        self.assertFalse(self.frac1 <= Frac(3, 8))
        self.assertTrue(self.frac1 <= Frac(2, 3))
        self.assertTrue(self.frac1 <= Frac(1, 1))
        self.assertTrue(Frac(2, 3) <= 1.5)
        self.assertTrue(Frac(3, 2) <= 1.5)
        self.assertFalse(Frac(5, 2) <= 1.5)

    def test_gt(self):
        self.assertTrue(self.frac1 > Frac(3, 8))
        self.assertFalse(self.frac1 > Frac(2, 3))
        self.assertFalse(self.frac1 > Frac(1, 1))
        self.assertFalse(Frac(2, 3) > 1.5)
        self.assertFalse(Frac(3, 2) > 1.5)
        self.assertTrue(Frac(5, 2) > 1.5)

    def test_ge(self):
        self.assertTrue(self.frac1 >= Frac(3, 8))
        self.assertTrue(self.frac1 >= Frac(2, 3))
        self.assertFalse(self.frac1 >= Frac(1, 1))
        self.assertFalse(Frac(2, 3) >= 1.5)
        self.assertTrue(Frac(3, 2) >= 1.5)
        self.assertTrue(Frac(5, 2) >= 1.5)

    def test_add(self):
        self.assertEqual(Frac(1, 2) + Frac(1, 3), Frac(5, 6))
        self.assertEqual(Frac(2, 4) + Frac(9, 15), Frac(11, 10))
        self.assertEqual(self.frac1 + self.zero, self.frac1)

    def test_sub(self):
        self.assertEqual(Frac(2, 3) - Frac(1, 3), Frac(1, 3))
        self.assertEqual(Frac(11, 10) - Frac(1, 2), Frac(3, 5))
        self.assertEqual(Frac(-1, 2) - Frac(11, 10), Frac(-8, 5))
        self.assertEqual(self.zero - self.frac1, -self.frac1)

    def test_mul(self):
        self.assertEqual(Frac(2, 3) * Frac(3, 5), Frac(2, 5))
        self.assertEqual(Frac(1, 2) * Frac(3, 5), Frac(3, 10))
        self.assertEqual(Frac(100, 2) * Frac(3, -50), Frac(-3, 1))
        self.assertEqual(self.zero * self.frac1, self.zero)

    def test_div(self):
        self.assertEqual(Frac(1, 2) / Frac(1, 3), Frac(3, 2))
        self.assertEqual(Frac(1, 2) / Frac(3, 5), Frac(5, 6))
        self.assertRaises(ZeroDivisionError, Frac.__truediv__, Frac(4, 9), self.zero)

    def test_floordiv(self):
        self.assertEqual(Frac(4, 3) // Frac(5, 4), 1)
        self.assertNotEqual(Frac(2) // Frac(200, 99), 1)
        self.assertRaises(ZeroDivisionError, Frac.__floordiv__, Frac(4, 9), self.zero)

    def test_mod(self):
        self.assertEqual(Frac(5, 2) % Frac(5, 4), self.zero)
        self.assertEqual(Frac(5, 2) % Frac(3, 4), Frac(1, 4))
        self.assertRaises(ZeroDivisionError, Frac.__mod__, Frac(5, 2), self.zero)

    def test_pos(self):
        self.assertEqual(+self.frac1, self.frac1)
        self.assertEqual(self.frac1 * Frac(8, 99) / Frac(8, 99), +self.frac1)

    def test_neq(self):
        self.assertEqual(-Frac(2, 5), Frac(-2, 5))

    def test_invert(self):
        self.assertEqual(Frac(1) / self.frac1, ~self.frac1)

    def test_float(self):
        self.assertAlmostEqual(float(Frac(2, 7)), 2 / 7)
        self.assertAlmostEqual(float(Frac(1, 5)), 0.2)
        self.assertAlmostEqual(float(Frac(1000, 3000)), 1 / 3)
        self.assertEqual(float(Frac(4, 2)), 2.0)

    def test_hash(self):
        self.assertEqual(hash(Frac(4, 6)), hash(Frac(2, 3)))
        self.assertEqual(hash(Frac(2)), hash(2.0))
        self.assertNotEqual(hash(Frac(3, 4)), hash(Frac(3, 5)))

    def test_is_positive(self):
        self.assertTrue(Frac(1, 2).is_positive())
        self.assertFalse(Frac(-3, 12).is_positive())
        self.assertFalse(Frac(2, -5).is_positive())

    def test_is_zero(self):
        self.assertTrue(self.zero.is_zero())
        self.assertFalse(Frac(3, 5).is_zero())
        self.assertTrue(Frac(0, 34).is_zero())


if __name__ == '__main__':
    unittest.main()
