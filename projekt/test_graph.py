#!usr/bin/env python3
from graph import Graph, dfs, bfs
import unittest


class TestGraph(unittest.TestCase):
    def setUp(self):
        self.g1 = Graph()
        self.g1.add_node(0)
        self.g1.add_node(1)
        self.g1.add_node(3)
        self.g1.add_edge(0, 1)
        self.g1.add_edge(0, 3)
        self.g1.add_edge(1, 3)

        self.g2 = Graph(isdirected=False)
        self.g2.add_node(0)
        self.g2.add_node(1)
        self.g2.add_node(3)
        self.g2.add_edge(0, 1)
        self.g2.add_edge(0, 3)
        self.g2.add_edge(1, 3)

        self.g3 = Graph.from_dict({0: 1, 1: {2}, 5: (1, 3, 3)})
        self.g4 = Graph(isdirected=False)
        self.g4.add_node(1)
        self.g4.add_edge(1, 1)

    def test_add_node(self):
        self.assertEqual(self.g1.nodes, [0, 1, 3])
        self.g1.add_node(1)
        self.assertEqual(self.g1.nodes, [0, 1, 3])
        self.g2.add_node(2)
        self.assertEqual(self.g2.nodes, [0, 1, 2, 3])
        self.g2.add_node(2)
        self.assertNotEqual(self.g2.nodes, [0, 1, 2, 2, 3])

    def test_remove_node(self):
        self.assertEqual(self.g1.nodes, [0, 1, 3])
        self.g1.remove_node(1)
        self.assertEqual(self.g1.nodes, [0, 3])
        self.assertNotEqual(self.g1.edges, [(0, 1), (0, 3), (1, 3)])
        self.assertEqual(self.g1.edges, [(0, 3)])
        self.g2.remove_node(2)
        self.assertEqual(self.g2.nodes, [0, 1, 3])
        self.assertEqual(self.g2.edges, [(0, 1), (0, 3), (1, 0), (1, 3), (3, 0), (3, 1)])
        self.g2.remove_node(1)
        self.assertEqual(self.g2.nodes, [0, 3])
        self.assertNotEqual(self.g2.edges, [(0, 1), (0, 3), (1, 0), (1, 3), (3, 0), (3, 1)])
        self.assertEqual(self.g2.edges, [(0, 3), (3, 0)])

    def test_add_edge(self):
        self.assertFalse(self.g1.has_edge(1, 2))
        self.assertRaises(ValueError, self.g1.add_edge, 1, 2)
        self.assertRaises(ValueError, self.g2.add_edge, 1, 4)
        self.g1.add_node(2)
        self.g1.add_edge(1, 2)
        self.assertTrue(self.g1.has_edge(1, 2))
        self.assertEqual(self.g1.edges, [(0, 1), (0, 3), (1, 2), (1, 3)])

    def test_remove_edge(self):
        self.g1.remove_edge(0, 1)
        self.assertEqual(self.g1.edges, [(0, 3), (1, 3)])
        self.g1.remove_edge(0, 2)
        self.assertEqual(self.g1.edges, [(0, 3), (1, 3)])
        self.g1.remove_edge(0, 3)
        self.assertEqual(self.g1.edges, [(1, 3)])
        self.g2.remove_edge(0, 1)
        self.assertEqual(self.g2.edges, [(0, 3), (1, 3), (3, 0), (3, 1)])
        self.g2.remove_edge(0, 2)
        self.assertEqual(self.g2.edges, [(0, 3), (1, 3), (3, 0), (3, 1)])
        self.g2.remove_edge(1, 3)
        self.assertEqual(self.g2.edges, [(0, 3), (3, 0)])

    def test_has_edge(self):
        self.assertTrue(self.g1.has_edge(0, 1))
        self.assertFalse(self.g1.has_edge(0, 4))
        self.assertFalse(self.g1.has_edge(1, 0))
        self.assertTrue(self.g2.has_edge(0, 1))
        self.assertFalse(self.g2.has_edge(0, 4))
        self.assertTrue(self.g2.has_edge(1, 0))
        self.assertTrue(self.g3.has_edge(0, 1))
        self.assertFalse(self.g3.has_edge(0, 2))

    def test_connections(self):
        self.assertEqual(self.g1.out_connections(0), {1, 3})
        self.assertEqual(self.g1.out_connections(1), {3})
        self.assertEqual(self.g1.in_connections(0), set())
        self.assertEqual(self.g1.in_connections(1), {0})
        self.assertEqual(self.g1.all_connections(0), {1, 3})
        self.assertEqual(self.g1.all_connections(1), {0, 3})
        self.assertEqual(self.g2.out_connections(0), {1, 3})
        self.assertEqual(self.g2.out_connections(1), {0, 3})
        self.assertEqual(self.g2.in_connections(0), {1, 3})
        self.assertEqual(self.g2.in_connections(1), {0, 3})
        self.assertEqual(self.g2.all_connections(0), {1, 3})
        self.assertEqual(self.g2.all_connections(1), {0, 3})
        self.assertEqual(self.g4.out_connections(1), {1})
        self.assertEqual(self.g4.in_connections(1), {1})
        self.assertEqual(self.g4.all_connections(1), {1})

        self.assertNotEqual(self.g1.out_connections(1), self.g1.all_connections(1))
        self.assertEqual(self.g2.out_connections(0), self.g2.all_connections(0))
        self.assertRaises(ValueError, self.g1.in_connections, 4)
        self.assertRaises(ValueError, self.g2.out_connections, 5)
        self.assertRaises(ValueError, self.g3.all_connections, 7)
        self.assertRaises(ValueError, self.g4.in_connections, 2)
        self.assertRaises(ValueError, self.g1.out_connections, 5)
        self.assertRaises(ValueError, self.g2.all_connections, 4)

    def test_graph_properties(self):
        self.assertEqual(self.g1.nodes, [0, 1, 3])
        self.assertEqual(self.g1.edges, [(0, 1), (0, 3), (1, 3)])
        self.assertTrue(self.g1.isdirected)
        self.assertEqual(self.g2.nodes, [0, 1, 3])
        self.assertEqual(self.g2.edges, [(0, 1), (0, 3), (1, 0), (1, 3), (3, 0), (3, 1)])
        self.assertFalse(self.g2.isdirected)
        self.assertEqual(self.g3.nodes, [0, 1, 2, 3, 5])
        self.assertEqual(self.g3.edges, [(0, 1), (1, 2), (5, 1), (5, 3)])
        self.assertTrue(self.g3.isdirected)
        self.assertEqual(self.g4.nodes, [1])
        self.assertEqual(self.g4.edges, [(1, 1)])
        self.assertFalse(self.g4.isdirected)

        test = Graph()
        self.assertEqual(test.nodes, [])
        self.assertEqual(test.edges, [])
        self.assertTrue(test.isdirected)

    def test_graph_getters(self):
        self.assertEqual(self.g1.getgraph(), {0: {1, 3}, 1: {3}, 3: set()})
        self.assertEqual(self.g1.vertnumber, 3)
        self.assertEqual(self.g2.getgraph(), {0: {1, 3}, 1: {0, 3}, 3: {0, 1}})
        self.assertEqual(self.g2.vertnumber, 3)
        self.assertEqual(self.g3.getgraph(),  {0: {1}, 1: {2}, 2: set(), 3: set(), 5: {1, 3}})
        self.assertEqual(self.g3.vertnumber, 5)
        self.assertEqual(self.g4.getgraph(), {1: {1}})
        self.assertEqual(self.g4.vertnumber, 1)

    def test_from_dict(self):
        self.assertIsInstance(Graph.from_dict(self.g3.getgraph()), Graph)
        self.assertIsInstance(Graph.from_dict({0: {1, 2}, 1: 2}), Graph)
        test = Graph()
        test.add_node(0)
        test.add_node(1)
        test.add_node(2)
        test.add_edge(0, 1)
        test.add_edge(0, 2)
        test.add_edge(1, 2)
        self.assertEqual(Graph.from_dict({0: {1, 2}, 1: 2}).getgraph(), test.getgraph())
        self.assertRaises(TypeError, Graph.from_dict, [0, 1, 2, (0, 1), (1, 2)])

    def test_dfs(self):
        self.assertEqual(dfs(self.g1, 0), [0, 1, 3])
        self.assertEqual(dfs(self.g1, 3), [3])
        self.assertRaises(ValueError, dfs, self.g1, 4)
        self.assertEqual(dfs(self.g2, 0), [0, 1, 3])
        self.assertEqual(dfs(self.g2, 1), [1, 0, 3])
        self.assertEqual(dfs(self.g3, 0), [0, 1, 2])
        self.assertEqual(dfs(self.g3, 5), [5, 1, 2, 3])
        self.assertEqual(dfs(self.g4, 1), [1])

    def test_bfs(self):
        self.assertEqual(bfs(self.g1, 0), {0: None, 1: 0, 3: 0})
        self.assertEqual(bfs(self.g1, 1), {1: None, 3: 1})
        self.assertEqual(bfs(self.g2, 0), {0: None, 1: 0, 3: 0})
        self.assertEqual(bfs(self.g2, 1), {1: None, 0: 1, 3: 1})
        self.assertEqual(bfs(self.g3, 0), {0: None, 1: 0, 2: 1})
        self.assertEqual(bfs(self.g3, 3), {3: None})
        self.assertEqual(bfs(self.g3, 5), {5: None, 1: 5, 2: 1, 3: 5})
        self.assertEqual(bfs(self.g4, 1), {1: None})
        self.assertRaises(ValueError, dfs, self.g4, 2)


if __name__ == '__main__':
    unittest.main()
