<h3>ZADANIE 6.1 (KLASA TIME)</h3>

<p>W pliku <em>times.py</em> zdefiniować klasę Time wraz z potrzebnymi metodami.
Odcinek czasu jest określony przez liczbę sekund.
Napisać kod testujący moduł <em>times</em>.

</p><hr><pre>class Time:
    """Klasa reprezentująca odcinek czasu."""

    def __init__(self, s=0):
        """Zwraca instancję klasy Time."""
        if s < 0:
            raise ValueError("ujemny czas")
        self.s = int(s)

    def __str__(self):
        """Zwraca string 'hh:mm:ss'."""
        h = self.s // 3600
        sec = self.s - h * 3600
        m = sec // 60
        sec = sec - m * 60
        return "{0:02d}:{1:02d}:{2:02d}".format(h, m, sec)

    def __repr__(self):
        """Zwraca string 'Time(s)'."""
        return "Time({})".format(self.s)

    def __add__(self, other):
        """Dodawanie odcinków czasu."""
        return Time(self.s + other.s)

    #def __cmp__(self, other): # Py2, porównywanie, -1|0|+1
    #    """Porównywanie odcinków czasu."""
    #    return cmp(self.s, other.s)

    # Py2.7 i Py3, rich comparisons.
    def __eq__(self, other):
        return self.s == other.s

    def __ne__(self, other):
        return self.s != other.s

    def __lt__(self, other):
        return self.s < other.s

    def __le__(self, other):
        return self.s <= other.s

    # nadmiarowe
    #def __gt__(self, other):
    #    return self.s > other.s

    # nadmiarowe
    #def __ge__(self, other):
    #    return self.s >= other.s

    def __int__(self):                  # int(time1)
        """Konwersja odcinka czasu do int."""
        return self.s

#Kod testujący moduł - dopisać co najmniej dwa testy do każdej sekcji.

import unittest

class TestTime(unittest.TestCase):

    def setUp(self):
        self.t1 = Time(3723)

    def test_print(self): pass      # test str() i repr()
        self.assertEqual(str(self.t1), "01:02:03")
        self.assertEqual(repr(self.t1), "Time(3723)")

    def test_cmp(self):
        # Trzeba sprawdzać ==, !=, >, >=, <, <=.
        self.assertTrue(Time(2) == Time(2))
        self.assertFalse(Time(2) == Time(3))
        self.assertTrue(Time(2) != Time(3))
        self.assertFalse(Time(2) != Time(2))
        self.assertTrue(Time(2) < Time(3))
        self.assertFalse(Time(4) < Time(3))
        self.assertTrue(Time(2) <= Time(3))
        self.assertFalse(Time(4) <= Time(3))
        self.assertTrue(Time(4) > Time(3))
        self.assertFalse(Time(2) > Time(3))
        self.assertTrue(Time(4) >= Time(3))
        self.assertFalse(Time(2) >= Time(3))

    def test_add(self):   # musi działać porównywanie
        self.assertEqual(Time(1) + Time(2), Time(3))

    def test_int(self): pass

    def tearDown(self): pass

if __name__ == "__main__":
    unittest.main()     # wszystkie testy
</pre><hr>

<h3>ZADANIE 6.2 (KLASA POINT)</h3>

<p>W pliku <em>points.py</em> zdefiniować klasę Point wraz z potrzebnymi metodami.
Punkty są traktowane jak wektory zaczepione w początku układu współrzędnych,
o końcu w położeniu (x, y). Napisać kod testujący moduł <em>points</em>.

</p><hr><pre>class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x, y):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self): pass         # zwraca string "(x, y)"

    def __repr__(self): pass        # zwraca string "Point(x, y)"

    def __eq__(self, other): pass   # obsługa point1 == point2

    def __ne__(self, other):        # obsługa point1 != point2
        return not self == other

    # Punkty jako wektory 2D.
    def __add__(self, other): pass  # v1 + v2

    def __sub__(self, other): pass  # v1 - v2

    def __mul__(self, other): pass  # v1 * v2, iloczyn skalarny, zwraca liczbę

    def cross(self, other):         # v1 x v2, iloczyn wektorowy 2D, zwraca liczbę
        return self.x * other.y - self.y * other.x

    def length(self): pass          # długość wektora

    def __hash__(self):
        return hash((self.x, self.y))   # bazujemy na tuple, immutable points

#Kod testujący moduł.

import unittest

class TestPoint(unittest.TestCase): pass
</pre><hr>

<h3>ZADANIE 6.3 (KLASA RECTANGLE)</h3>

<p>W pliku <em>rectangles.py</em> zdefiniować klasę Rectangle
wraz z potrzebnymi metodami.
Prostokąt jest określony przez podanie dwóch wierzchołków,
lewego dolnego i prawego górnego.
Napisać kod testujący moduł <em>rectangles</em>.</p>

<hr><pre>
from points import Point

class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self): pass         # "[(x1, y1), (x2, y2)]"

    def __repr__(self): pass        # "Rectangle(x1, y1, x2, y2)"

    def __eq__(self, other): pass   # obsługa rect1 == rect2

    def __ne__(self, other):        # obsługa rect1 != rect2
        return not self == other

    def center(self): pass          # zwraca środek prostokąta

    def area(self): pass            # pole powierzchni

    def move(self, x, y): pass      # przesunięcie o (x, y)

#Kod testujący moduł.

import unittest

class TestRectangle(unittest.TestCase): pass
</pre><hr>

<h3>ZADANIE 6.5 (KLASA FRAC)</h3>

<p>W pliku <em>fracs.py</em> zdefiniować klasę Frac
wraz z potrzebnymi metodami.
Ułamek jest reprezentowany przez parę liczb całkowitych.
Napisać kod testujący moduł <em>fracs</em>.

</p><hr><pre>class Frac:
    """Klasa reprezentująca ułamek."""

    def __init__(self, x=0, y=1):
        self.x = x
        self.y = y

    def __str__(self): pass         # zwraca "x/y" lub "x" dla y=1

    def __repr__(self): pass        # zwraca "Frac(x, y)"

    #def __cmp__(self, other): pass  # cmp(frac1, frac2)    # Py2

    def __eq__(self, other): pass    # Py2.7 i Py3

    def __ne__(self, other): pass

    def __lt__(self, other): pass

    def __le__(self, other): pass

    #def __gt__(self, other): pass

    #def __ge__(self, other): pass

    def __add__(self, other): pass  # frac1 + frac2

    def __sub__(self, other): pass  # frac1 - frac2

    def __mul__(self, other): pass  # frac1 * frac2

    def __div__(self, other): pass  # frac1 / frac2, Py2

    def __truediv__(self, other): pass  # frac1 / frac2, Py3

    def __floordiv__(self, other): pass  # frac1 // frac2, opcjonalnie

    def __mod__(self, other): pass  # frac1 % frac2, opcjonalnie

    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self):  # -frac = (-1)*frac
        return Frac(-self.x, self.y)

    def __invert__(self):  # odwrotnosc: ~frac
        return Frac(self.y, self.x)

    def __float__(self): pass       # float(frac)

    def __hash__(self):
        return hash(float(self))   # immutable fracs
        # w Pythonie set([2]) == set([2.0])
        # chcemy set([2]) == set([Frac(2)])

#Kod testujący moduł.

import unittest

class TestFrac(unittest.TestCase): pass
</pre><hr>

