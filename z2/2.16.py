#!usr/bin/env python3

if __name__ == '__main__':
    line = 'Python was created by GvR, and first released on February 20, 1991'

    expected_result = ('Python was created by Guido van Rossum, and first'
                       ' released on February 20, 1991')
    result = line.replace('GvR', 'Guido van Rossum')
    assert result == expected_result

    # alternatywnie:
    # import re
    # re.sub("GvR", "Guido Van Rossum", line)
