#!usr/bin/env python3

from points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1, y1, x2, y2):
        if x1 >= x2 or y1 >= y2:
            raise ValueError("The first point must be lower left point, the second upper right point")
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return f"[({self.pt1.x}, {self.pt1.y}), ({self.pt2.x}, {self.pt2.y})]"

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return f"Rectangle({self.pt1.x}, {self.pt1.y}, {self.pt2.x}, {self.pt2.y})"

    def __eq__(self, other):  # obsługa rect1 == rect2
        if not isinstance(other, Rectangle):
            return False

        return self.pt1 == other.pt1 and self.pt2 == other.pt2

    def __ne__(self, other):  # obsługa rect1 != rect2
        return not self == other

    def center(self):  # zwraca środek prostokąta
        return Point((self.pt1.x + self.pt2.x) / 2, (self.pt1.y + self.pt2.y) / 2)

    def area(self):  # pole powierzchni
        return (self.pt2.x - self.pt1.x) * (self.pt2.y - self.pt1.y)

    def move(self, x, y):  # przesunięcie o (x, y)
        self.pt1.x += x
        self.pt2.x += x
        self.pt1.y += y
        self.pt2.y += y

    def intersection(self, other):  # część wspólna prostokątów
        if self == other:
            return self
        elif self.area() > other.area():
            biggerrect = self
            smallerrect = other
        else:
            biggerrect = other
            smallerrect = self

        points = [smallerrect.pt1, smallerrect.pt2, Point(smallerrect.pt1.x, smallerrect.pt2.y),
                  Point(smallerrect.pt2.x, smallerrect.pt1.y)]

        for point in points:
            if (biggerrect.pt1.x < point.x < biggerrect.pt2.x) and (biggerrect.pt1.y < point.y < biggerrect.pt2.y):
                x1 = max(biggerrect.pt1.x, smallerrect.pt1.x)
                y1 = max(biggerrect.pt1.y, smallerrect.pt1.y)
                x2 = min(biggerrect.pt2.x, smallerrect.pt2.x)
                y2 = min(biggerrect.pt2.y, smallerrect.pt2.y)
                return Rectangle(x1, y1, x2, y2)

    def cover(self, other):  # prostokąt nakrywający oba
        x1 = min(self.pt1.x, other.pt1.x)
        y1 = min(self.pt1.y, other.pt1.y)
        x2 = max(self.pt2.x, other.pt2.x)
        y2 = max(self.pt2.y, other.pt2.y)
        return Rectangle(x1, y1, x2, y2)

    def make4(self):  # zwraca krotkę czterech mniejszych
        center = self.center()
        rec1 = Rectangle(self.pt1.x, self.pt1.y, center.x, center.y)
        rec2 = Rectangle(center.x, self.pt1.y, self.pt2.x, center.y)
        rec3 = Rectangle(self.pt1.x, center.y, center.x, self.pt2.y)
        rec4 = Rectangle(center.x, center.y, self.pt2.x, self.pt2.y)
        return rec1, rec2, rec3, rec4


# Kod testujący moduł.
import unittest


class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.r1 = Rectangle(0, 0, 3, 4)
        self.r2 = Rectangle(0, 0, 3, 4)
        self.r3 = Rectangle(0, 0, 5, 5)
        self.r4 = Rectangle(1, 2, 3, 4)

    def test_params(self):
        self.assertRaises(ValueError, Rectangle, 1, 2, 2, 2)
        self.assertRaises(ValueError, Rectangle, 1, 3, 2, 2)
        self.assertRaises(ValueError, Rectangle, 1, 2, 0, 2)
        self.assertRaises(ValueError, Rectangle, 1, 2, 1, 3)
        self.assertRaises(ValueError, Rectangle, 1, 2, 1, 2)

    def test_str(self):
        self.assertEqual(str(self.r1), "[(0, 0), (3, 4)]")
        self.assertEqual(str(self.r3), "[(0, 0), (5, 5)]")
        self.assertEqual(str(self.r4), "[(1, 2), (3, 4)]")

    def test_repr(self):
        self.assertEqual(repr(self.r1), "Rectangle(0, 0, 3, 4)")
        self.assertEqual(repr(self.r3), "Rectangle(0, 0, 5, 5)")
        self.assertEqual(repr(self.r4), "Rectangle(1, 2, 3, 4)")

    def test_eq(self):
        self.assertEqual(self.r1, self.r1)
        self.assertEqual(self.r1, self.r2)
        self.assertNotEqual(self.r1, self.r3)
        self.assertNotEqual(self.r3, self.r4)

    def test_center(self):
        self.assertEqual(self.r1.center(), Point(1.5, 2))
        self.assertEqual(self.r3.center(), Point(2.5, 2.5))
        self.assertEqual(self.r4.center(), Point(2, 3))
        self.assertNotEqual(self.r4.center(), Point(1, 2))

    def test_area(self):
        self.assertEqual(self.r1.area(), 12)
        self.assertNotEqual(self.r2.area(), 10)
        self.assertEqual(self.r3.area(), 25)
        self.assertEqual(self.r4.area(), 4)

    def test_move(self):
        self.r1.move(2, 3)
        self.r1.move(2, 3)
        self.r3.move(2, 3)
        self.r4.move(2, 3)
        self.assertNotEqual(self.r1, Rectangle(2, 3, 5, 7))
        self.assertEqual(self.r1, Rectangle(4, 6, 7, 10))
        self.assertEqual(self.r3, Rectangle(2, 3, 7, 8))
        self.assertEqual(self.r4, Rectangle(3, 5, 5, 7))

    def test_intersect(self):
        self.assertEqual(self.r1.intersection(self.r2), self.r1)
        self.assertEqual(self.r1.intersection(self.r3), self.r1)
        self.assertEqual(self.r3.intersection(self.r4), self.r4)
        self.assertEqual(Rectangle(0, 0, 1, 1).intersection(Rectangle(1, 1, 3, 3)),
                         None)
        self.assertEqual(self.r4.intersection(Rectangle(8, 9, 15, 16)), None)
        self.assertEqual(self.r1.intersection(Rectangle(1, 2, 5, 6)), self.r4)
        self.assertEqual(Rectangle(1, 2, 5, 6).intersection(self.r1), self.r4)

    def test_cover(self):
        self.assertEqual(self.r1.cover(self.r2), self.r1)
        self.assertEqual(self.r1.cover(self.r3), self.r3)
        self.assertEqual(self.r3.cover(self.r1), self.r3)
        self.assertEqual(Rectangle(0, 0, 1, 1).cover(Rectangle(2, 2, 3, 3)),
                         Rectangle(0, 0, 3, 3))
        self.assertEqual(Rectangle(-2, 0, 1, 2).cover(Rectangle(1, 0, 3, 2)),
                         Rectangle(-2, 0, 3, 2))

    def test_make4(self):
        self.assertEqual(self.r1.make4(), (Rectangle(0, 0, 1.5, 2), Rectangle(1.5, 0, 3, 2),
                                           Rectangle(0, 2, 1.5, 4), Rectangle(1.5, 2, 3, 4)))
        self.assertEqual(self.r4.make4(), (Rectangle(1, 2, 2, 3), Rectangle(2, 2, 3, 3),
                                           Rectangle(1, 3, 2, 4), Rectangle(2, 3, 3, 4)))


if __name__ == '__main__':
    unittest.main()
