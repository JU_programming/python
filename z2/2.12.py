#!usr/bin/env python3

if __name__ == '__main__':
    line = 'Lorem ipsum dolor sit amet consectetur'

    expected_result = 'Lidsac'
    expected_result2 = 'mmrttr'
    result = ''
    result2 = ''

    for x in line.split():
        result = result + x[0]
        result2 = result2 + x[-1]

    assert result == expected_result
    assert result2 == expected_result2

    # alternatywnie:
    # "".join(word[0] for word in line.split()) # generator
    # lub też map z wyr. lambda
