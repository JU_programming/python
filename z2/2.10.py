#!usr/bin/env python3

if __name__ == '__main__':
    line = 'aaa bbb\tccc\nddd eee\t fff\n'
    expected_result = 6
    result = len(line.split())
    assert result == expected_result
