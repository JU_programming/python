#!usr/bin/env python3

"""
================================================================================
Autor: Pawel Konefal
================================================================================

Implementacja grafu wraz z:
        Graph(isdirected) - inicjalizuje graf skierowany/nieskierowany
        add_node(node) - dodaje wierzcholek node do grafu
        remove_node(node) - usuwa wierzcholek node z grafu
                            wraz z wszystkimi polaczeniami z/do wierzcholka
        add_edge(i, j) - dodaje krawedz pomiedzy wierzcholkami i oraz j
                         (dla grafu nieskierowanego rowniez w druga strone)
        remove_edge(i, j) - usuwa krawedz pomiedzy wierzcholkami i oraz j
                            (dla grafu nieskierowanego rowniez w druga strone)
        has_edge(i, j) - sprawdza, czy krawedz
                         pomiedzy wierzcholkami i oraz j istnieje
        in_connections(node) - zwraca wszystkie wierzcholki od ktorych istnieja
                            polaczenia do wierzcholka node
        out_connections(node) - zwraca wszystkie wierzcholki, do ktorych prowadza
                             krawedzie wychodzace z node
        all_connections(node) - zwraca wszystkie wierzcholki ktore sa polaczone
                             z node lub z ktorymi node jest polaczony
        nodes() - zwraca wszystkie wierzcholki grafu
        edges() - zwraca liste wszystkich krawedzi w grafie
        print_graph() - wypisuje graf w terminalu
        vertnumber() - zwraca liczbe wierzcholkow
        getgraph() - zwraca wierzcholki wraz z wszystkimi wierzcholkami, do ktorych
                     wychodza krawedzie
        isdirected() - sprawdzenie, czy graf jest skierowany
        from_dict(graph_dict, isdirected) - stworzenie grafu ze slownika graph_dict

        dfs(graph, startvert) - przeszukiwanie grafu w glab,
                                zwraca wierzcholki w kolejnosci przegladania
        bfs(graph, startvert) - przeszukiwanie grafu wszerz, zwraca rodzicow
"""

import collections


class Graph:
    def __init__(self, isdirected=True):
        """Konstruktor klasy Graph

        :param isdirected: wybor pomiedzy grafem skierowanym, a nieskierowanym
        :type isdirected: bool
        """
        self.__directed = isdirected
        self.__vec = {}

    def add_node(self, node):
        """Wstawia wierzcholek do grafu

        :param node: wierzcholek do dodania
        :type node: int
        :return: None
        :rtype: None
        """
        if node not in self.__vec:
            self.__vec[node] = set()

    def remove_node(self, node):
        """Usuwa wierzcholek z grafu wraz z wszystkimi krawedziami
        z/do wierzcholka

        :param node: wierzcholek do usuniecia
        :type node: int
        :return: None
        :rtype: None
        """
        if node in self.__vec:
            temp = self.in_connections(node).copy()
            [self.remove_edge(i, node) for i in temp]
            del self.__vec[node]

    def add_edge(self, i, j):
        """Dodaje krawedz pomiedzy wierzcholkami i oraz j
        (dla grafu nieskierowanego rowniez w druga strone)

        :param i: wierzcholek poczatkowy krawedzi
        :type i: int
        :param j: wierzcholek koncowy krawedzi
        :type j: int
        :raises ValueError: Przynajmniej jeden z wierzcholkow nie nalezy do grafu
        :return: None
        :rtype: None
        """
        if i in self.__vec and j in self.__vec:
            self.__vec[i].add(j)
            if not self.__directed:
                self.__vec[j].add(i)
        else:
            raise ValueError("One of the vertices doesn't exist")

    def remove_edge(self, i, j):
        """Usuwa krawedz pomiedzy wierzcholkami i oraz j
        (dla grafu nieskierowanego rowniez w druga strone)

        :param i: wierzcholek poczatkowy krawedzi
        :type i: int
        :param j: wierzcholek koncowy krawedzi
        :type j: int
        :return: None
        :rtype: None
        """
        if self.has_edge(i, j):
            self.__vec[i].remove(j)
            if not self.__directed:
                self.__vec[j].remove(i)

    def has_edge(self, i, j):
        """Sprawdza, czy krawedz pomiedzy wierzcholkami i oraz j istnieje

        :param i: wierzcholek poczatkowy krawedzi
        :type i: int
        :param j: wierzcholek koncowy krawedzi
        :type j: int
        :return: Informacje, czy dana krawedz istnieje
        :rtype: bool
        """
        return True if i in self.__vec and j in self.__vec[i] else False

    def in_connections(self, node):
        """Zwraca wszystkie wierzcholki od ktorych sa polaczenia do wierzcholka node

        :param node: wierzcholek
        :type node: int
        :raises ValueError: Podano zly wierzcholek
        :return: Wszystkie wierzcholki, z ktorych krawedz prowadzi do node
        :rtype: set
        """
        if node in self.__vec:
            # Kiedy graf jest nieskierowany, krawedzie wychodzace i wchodzace do
            # danego wierzcholka sa takie same
            if not self.__directed:
                return self.out_connections(node)

            result = set()
            [result.add(i) for i in self.__vec if node in self.__vec[i]]
        else:
            raise ValueError("Vertex doesn't exist")

        return result

    def out_connections(self, node):
        """Zwraca wszystkie wierzcholki do ktorych sa polaczenia z wierzcholka node

        :param node: wierzcholek
        :type node: int
        :raises ValueError: Podano zly wierzcholek
        :return: Wszystkie wierzcholki, do ktorych krawedz prowadzi z node
        :rtype: set
        """
        if node not in self.__vec:
            raise ValueError("Vertex doesn't exist")

        return self.__vec[node]

    def all_connections(self, node):
        """Zwraca sume zbiorow krawedzi wychodzacych i wchodzacych do node

        :param node: wierzcholek
        :type node: int
        :return: Wszystkie wierzcholki, z/do ktorych krawedz prowadzi do/z node
        :rtype: set
        """
        return self.out_connections(node).union(self.in_connections(node))

    @property
    def nodes(self):
        """Zwraca wszystkie wierzcholki w grafie

        :return: Liste z wszystkimi wierzcholkami grafu
        :rtype: list
        """
        return list(sorted(self.__vec.keys()))

    @property
    def edges(self):
        """Zwraca liste krawedzi (2-krotek) grafu

        :return: Lista krawedzi w grafie
        :rtype: list[tuple[int, int]]
        """
        result = []
        for source in self.__vec:
            for target in self.__vec[source]:
                result.append((source, target))
        return result

    def print_graph(self):
        """Wypisuje postac grafu na ekranie

        :return: None
        """
        result = []
        for source in self.__vec:
            result.append("{} : ".format(source))
            for target in self.__vec[source]:
                result.append("{} ".format(target))
            result.append("\n")
        print("".join(result))

    @property
    def vertnumber(self):
        """Zwraca liczbe wierzcholkow w grafie

        :return: Liczba wierzcholkow w grafie
        :rtype: int
        """
        return len(self.nodes)

    def getgraph(self):
        """Zwraca wierzcholki wraz z wszystkimi wierzcholkami, do ktorych
        wychodza krawedzie

        :return: Slownik, gdzie kluczem jest wierzcholek, a wartoscia
                 zbior wierzcholkow, do ktorych sa krawedzie wychodzace
        :rtype: dict [int, set]
        """
        return self.__vec

    @property
    def isdirected(self):
        """Sprawdza rodzaj grafu

        :return: Informacje, czy graf jest skierowany
        :rtype: bool
        """
        return self.__directed

    @classmethod
    def from_dict(cls, graph_dict, isdirected=True):
        """Tworzy graf z podanego slownika

        :param graph_dict: Slownik z kluczem jako wierzcholkiem oraz wartoscia
                    jako zbiorem wierzcholkow, do ktorych jest krawedz z klucza
        :type graph_dict: dict
        :param isdirected: Okreslenie, czy graf jest skierowany
        :type isdirected: bool
        :raises TypeError: graph_dict nie jest slownikiem
        :return: nowy obiekt klasy Graph
        :rtype: Graph
        """
        if not isinstance(graph_dict, dict):
            raise TypeError("It is not a dictionary")

        g = cls(isdirected)
        for key in graph_dict.keys():
            g.add_node(key)
            # jesli to pojedyncza liczba, nie da sie przez nia iterowac,
            # wiec trzeba ja dodac od razu
            try:
                for value in graph_dict[key]:
                    g.add_node(value)
                    g.add_edge(key, value)
            except TypeError:
                g.add_node(graph_dict[key])
                g.add_edge(key, graph_dict[key])

        return g


def dfs(graph, startvert):
    """Przeszukiwanie grafu w glab

    :param graph: graf, ktory chcemy przejrzec
    :type graph: Graph
    :param startvert: wierzcholek poczatkowy
    :type startvert: int
    :raises ValueError: podany wierzcholek nie nalezy do grafu
    :return: Lista wierzcholkow w kolejnosci dfs
    :rtype: list
    """
    def dfs_visit(_g, _v):
        """Funkcja pomocnicza dla dfs, sluzy do przegladania wierzcholkow"""
        nonlocal visited_vert
        visited_vert.append(_v)
        for u in _g.out_connections(_v):
            if u not in visited_vert:
                dfs_visit(_g, u)

    if startvert not in graph.nodes:
        raise ValueError("Starting vertex doesn't exist")

    visited_vert = []
    dfs_visit(graph, startvert)

    return visited_vert


def bfs(graph, startvert):
    """Przeszukiwanie grafu wszerz

    :param graph: graf, ktory chcemy przejrzec
    :type graph: Graph
    :param startvert: wierzcholek poczatkowy
    :type startvert: int
    :raises ValueError: podany wierzcholek nie nalezy do grafu
    :return: Slownik z wierzcholkami jako kluczami i rodzicami jako wartosciami
    :rtype: dict
    """
    if startvert not in graph.nodes:
        raise ValueError("Starting vertex doesn't exist")

    parent = {startvert: None}
    q = collections.deque()
    q.append(startvert)

    while q:
        vert = q.popleft()
        for node in graph.out_connections(vert):
            if node not in parent:  # wyszukiwanie O(1)
                parent[node] = vert
                q.append(node)

    return parent
