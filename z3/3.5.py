#!usr/bin/env python3

if __name__ == '__main__':
    length = 18

    if length > 9999:
        print("Miarka przy podanej dlugosci stanie sie nieczytelna. Wybierz"
              " mniejsza liczbe")
    else:
        ruler_string = "....".join(['|'] * (length + 1))
        ruler_string_numbers = '0' + "".join([str(i + 1).rjust(5) for i in range(length)])

        # print("{}\n{}".format(measure_string, measure_string_numbers))
        # print(f"{measure_string}\n{measure_string_numbers}")
        ruler = ruler_string + '\n' + ruler_string_numbers + '\n'
        print(ruler)

        # mozna tez z wykorzystaniem extend oraz append
        # ruler = ["....".join("|" * (length + 1)), "\n0"]
        # ruler.extend(str(i + 1).rjust(5) for i in range(length))
        # ruler.append("\n")
        # print("".join(ruler))
