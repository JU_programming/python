#!usr/bin/env python3

import random
import unittest


# z pomoca funkcji iter
# it = (x for _ in iter(int, 1) for x in iter(range(2)))
def mod2_iterator():
    while True:
        yield 0
        yield 1


# z pomoca funkcji iter
# it = (random.choice(["N", "E", "S", "W"]) for _ in iter(int, 1))
def random_direction():
    while True:
        yield random.choice(["N", "E", "S", "W"])


# z pomoca funkcji iter
# it = (x for _ in iter(int, 1) for x in iter(range(7)))
def weekday_iterator():
    i = -1
    while True:
        i = (i + 1) % 7
        yield i


class TestIter(unittest.TestCase):
    def setUp(self):
        self.i1 = mod2_iterator()
        self.i2 = random_direction()
        self.i3 = weekday_iterator()
        self.i4 = (x for _ in iter(int, 1) for x in iter(range(2)))  # rownowazne self.i1
        self.i5 = (random.choice(["N", "E", "S", "W"]) for _ in iter(int, 1))  # rownowazne self.i2
        self.i6 = (x for _ in iter(int, 1) for x in iter(range(7)))  # rownowazne self.i3

    def test_mod2_iter(self):
        for i in range(2 * 2):
            with self.subTest(i=i):
                self.assertEqual(next(self.i1), i % 2)
                self.assertEqual(next(self.i4), i % 2)

    def test_rand_direction(self):
        for i in range(10):
            with self.subTest(i=i):
                self.assertIn(next(self.i2), ["N", "E", "S", "W"])
                self.assertIn(next(self.i5), ["N", "E", "S", "W"])

    def test_weekday_iter(self):
        for i in range(2 * 7):
            with self.subTest(i=i):
                self.assertEqual(next(self.i3), i % 7)
                self.assertEqual(next(self.i6), i % 7)


if __name__ == "__main__":
    unittest.main()
