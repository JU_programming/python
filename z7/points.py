#!usr/bin/env python3

import math


class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x, y):  # konstruktor
        self.x = x
        self.y = y

    def __str__(self):
        return f"({self.x}, {self.y})"         # zwraca string "(x, y)"

    def __repr__(self):
        return f"Point({self.x}, {self.y})"        # zwraca string "Point(x, y)"

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y  # obsługa point1 == point2

    def __ne__(self, other):
        return not self == other  # obsługa point1 != point2

    # Punkty jako wektory 2D.
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)  # v1 + v2

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)  # v1 - v2

    def __mul__(self, other):
        return self.x * other.x + self.y * other.y  # v1 * v2, iloczyn skalarny, zwraca liczbę

    def cross(self, other):         # v1 x v2, iloczyn wektorowy 2D, zwraca liczbę
        return self.x * other.y - self.y * other.x

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)  # długość wektora

    def __hash__(self):
        return hash((self.x, self.y))  # bazujemy na tuple, immutable points


# Kod testujący moduł.
import unittest


class TestPoint(unittest.TestCase):
    def setUp(self):
        self.p_zero = Point(0, 0)
        self.p1 = Point(3, 4)
        self.p2 = Point(5, 5)
    
    def test_print(self):
        self.assertEqual(str(self.p_zero), "(0, 0)")
        self.assertEqual(str(self.p1), "(3, 4)")
        self.assertEqual(str(self.p2), "(5, 5)")
        self.assertEqual(repr(self.p_zero), "Point(0, 0)")
        self.assertEqual(repr(self.p1), "Point(3, 4)")
        self.assertEqual(repr(self.p2), "Point(5, 5)")

    def test_cmp(self):
        self.assertTrue(self.p1 == Point(3, 4))
        self.assertFalse(self.p_zero == self.p2)
        self.assertTrue(self.p1 != self.p2)
        self.assertFalse(self.p_zero != Point(0, 0))

    def test_add(self):
        self.assertEqual(self.p1 + self.p2, Point(8, 9))
        self.assertEqual(self.p_zero + self.p1, self.p1)
        self.assertNotEqual(self.p1 + self.p2, self.p_zero)

    def test_sub(self):
        self.assertEqual(self.p1 - self.p2, Point(-2, -1))
        self.assertNotEqual(self.p2 - self.p1, self.p_zero)
        self.assertEqual(self.p2 - self.p1, Point(2, 1))

    def test_mul(self):
        self.assertEqual(self.p1 * self.p2, 35)
        self.assertEqual(self.p_zero * self.p1, 0)
        self.assertEqual(self.p2 * Point(-1, 1), 0)

    def test_cross(self):
        self.assertEqual(self.p1.cross(self.p2), -5)
        self.assertNotEqual(self.p2.cross(self.p1), -5)
        self.assertEqual(self.p2.cross(self.p1), 5)

    def test_length(self):
        self.assertEqual(self.p_zero.length(), 0)
        self.assertEqual(self.p1.length(), 5)
        self.assertEqual(self.p2.length(), 5 * math.sqrt(2))

    def test_hash(self):
        self.assertEqual(hash(self.p1), hash((3, 4)))
        self.assertNotEqual(hash(self.p_zero), hash(self.p2))


if __name__ == '__main__':
    unittest.main()
