#!usr/bin/env python3

if __name__ == '__main__':
    expected_result = [1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26,
                       28, 29]
    L = []
    [L.append(i) for i in range(31) if i % 3]
    assert L == expected_result
